﻿using System;
using System.IO;
using System.Drawing;
using NET_Launcher.Classes;
using System.Windows.Forms;

namespace NET_Launcher.Forms
{
    public partial class ContentForm : Form
    {
        public ContentForm()
        {
            InitializeComponent();
        }

        private void ContentForm_Load(object sender, EventArgs e)
        {
            if (File.Exists(Vars.chrFile)) editorButton.Enabled = true;

            gameButton.BackgroundImage = Image.FromFile(Vars.conPath + "GameDownload.png");
            charButton.BackgroundImage = Image.FromFile(Vars.conPath + "CharDownload.png");
            stageButton.BackgroundImage = Image.FromFile(Vars.conPath + "StageDownload.png");
            editorButton.BackgroundImage = Image.FromFile(Vars.conPath + "CharEditor.png");
        }

        private void gameButton_Click(object sender, EventArgs e)
        {
            DownloadGame downloadGame = new DownloadGame();

            downloadGame.ShowDialog(this);
        }

        private void editorButton_Click(object sender, EventArgs e)
        {
            CharEditor charEditor = new CharEditor();

            charEditor.ShowDialog(this);
        }
    }
}

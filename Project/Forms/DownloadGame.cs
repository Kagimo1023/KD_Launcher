﻿using System;
using System.ComponentModel;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using NET_Launcher.Classes;

namespace NET_Launcher.Forms
{
    public partial class DownloadGame : Form
    {
        Uri radioLink = new Uri("http://google.ru/");

        string fileName = "";

        bool deleteOldLauncher;

        public DownloadGame()
        {
            InitializeComponent();
            InitializeRadioBoxes();
            japRadio.CheckedChanged += new EventHandler(japRadio_CheckedChanged);
            engRadio.CheckedChanged += new EventHandler(engRadio_CheckedChanged);
            dirBox.TextChanged += new EventHandler(dirBox_TextChanged);
            dirBox.Text = Application.StartupPath;
            sizeEN.Text = Methods.GetFileSize(Vars.en_Link);
            sizeJP.Text = Methods.GetFileSize(Vars.jp_Link);
        }

        public void InitializeRadioBoxes()
        {
            radioLink = Vars.jp_Link;
            fileName = "KD_JAP.zip";
        }

        private void dirBox_TextChanged(object sender, EventArgs e)
        {
            if (dirBox.Text != Application.StartupPath)
            {
                checkBox1.Enabled = true;
            }
            else
            {
                checkBox1.Enabled = false;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            deleteOldLauncher = true;
        }

        private void engRadio_CheckedChanged(object sender, EventArgs e)
        {
            radioLink = Vars.en_Link;
            fileName = "KD_ENG.zip";
        }

        private void japRadio_CheckedChanged(object sender, EventArgs e)
        {
            radioLink = Vars.jp_Link;
            fileName = "KD_JAP.zip";
        }

        private void downloadButton_Click(object sender, EventArgs e)
        {
            downloadButton.Enabled = false;
            cancelButton.Enabled = false;
            folderButton.Enabled = false;

            japRadio.Enabled = false;
            engRadio.Enabled = false;

            sizeEN.Enabled = false;
            sizeJP.Enabled = false;

            dirBox.Visible = false;
            statusBar.Visible = true;

            dirLabel.Text = "Status:";
            statusBar.Text = "Trying connect to site...";

            Methods.downloadFile(radioLink, fileName, client_DownloadFileCompleted, client_DownloadProgressChanged);
        }

        void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            progressBar.Visible = false;

            if (File.Exists(fileName))
            {
                statusBar.Visible = true;
                statusBar.Update();

                statusBar.Text = "Archive in extract process...";
                statusBar.Update();
                statusBar.Refresh();

                Methods.ExtractZipFile(fileName, "", dirBox.Text);

                if (deleteOldLauncher == true)
                {
                    statusBar.Text = "Launcher copy to game directory process...";
                    statusBar.Update();
                    System.Threading.Thread.Sleep(1500);

                    Methods.DirectoryCopy("Launcher", dirBox.Text + "\\Launcher", true);
                    File.Copy(Application.ExecutablePath, dirBox.Text + "\\NET Kakutou Dentsesu Launcher.exe");
                    File.Copy("ICSharpCode.SharpZipLib.dll", dirBox.Text + "\\ICSharpCode.SharpZipLib.dll");
                    File.Copy(Vars.lnrPath, dirBox.Text + "\\Launcher.ini");

                    statusBar.Text = "Game was download! Launcher now is restart!";
                    System.Threading.Thread.Sleep(2000);
                    Process.Start(dirBox.Text + "\\NET Kakutou Dentsesu Launcher.exe");
                    Process.Start(new ProcessStartInfo()
                    {
                        Arguments = "/C choice /C Y /N /D Y /T 3 & Del \"" + Application.ExecutablePath + "\"",
                        WindowStyle = ProcessWindowStyle.Hidden,
                        CreateNoWindow = true,
                        FileName = "cmd.exe"
                    });
                    Process.Start(new ProcessStartInfo()
                    {
                        Arguments = "/C choice /C Y /N /D Y /T 3 & Del \"" + Vars.lnrPath + "\"",
                        WindowStyle = ProcessWindowStyle.Hidden,
                        CreateNoWindow = true,
                        FileName = "cmd.exe"
                    });
                    Process.Start(new ProcessStartInfo()
                    {
                        Arguments = "/C choice /C Y /N /D Y /T 3 & Del \"" + Application.StartupPath + "\\ICSharpCode.SharpZipLib.dll" + "\"",
                        WindowStyle = ProcessWindowStyle.Hidden,
                        CreateNoWindow = true,
                        FileName = "cmd.exe"
                    });
                    Process.Start(new ProcessStartInfo()
                    {
                        Arguments = "/C rd /s /q \"" + Application.StartupPath + "\\Launcher" + "\"",
                        WindowStyle = ProcessWindowStyle.Hidden,
                        CreateNoWindow = true,
                        FileName = "cmd.exe"
                    });
                    Application.Exit();
                } else
                {
                    statusBar.Text = "Game was download! Launcher is restarting!";
                    statusBar.Update();
                    System.Threading.Thread.Sleep(5000);

                    Update();
                    Application.Restart();
                }

                downloadButton.Visible = true;
                folderButton.Visible = true;
                cancelButton.Visible = true;

                statusBar.Visible = false;
                dirBox.Visible = true;
            }
        }
        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            dirBox.Visible = false;
            statusBar.Visible = true;
            statusBar.Text = "Download file: " + e.ProgressPercentage + "% " + "(" + e.BytesReceived + " / " + e.TotalBytesToReceive + ")";

            progressBar.BringToFront();
            progressBar.Visible = true;
            progressBar.Maximum = (int)e.TotalBytesToReceive / 100;
            progressBar.Value = (int)e.BytesReceived / 100;

            downloadButton.Visible = false;
            cancelButton.Visible = false;
            folderButton.Visible = false;
        }

        private void folderButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.SelectedPath = dirBox.Text;
            folderBrowserDialog.Description = "Select folder for download game";
            folderBrowserDialog.ShowDialog();
            dirBox.Text = folderBrowserDialog.SelectedPath;
            if(dirBox.Text != Application.StartupPath)
            {
                checkBox1.Enabled = true;
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

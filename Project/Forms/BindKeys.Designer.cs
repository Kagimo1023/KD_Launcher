﻿namespace NET_Launcher.Forms
{
    partial class bindForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(bindForm));
            this.oldGroup = new System.Windows.Forms.GroupBox();
            this.oldCharText = new System.Windows.Forms.Label();
            this.oldCodeText = new System.Windows.Forms.Label();
            this.oCharText = new System.Windows.Forms.Label();
            this.oCodeText = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.newGroup = new System.Windows.Forms.GroupBox();
            this.newCodeText = new System.Windows.Forms.Label();
            this.newCharText = new System.Windows.Forms.Label();
            this.nCodeText = new System.Windows.Forms.Label();
            this.nCharText = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.bindGroup = new System.Windows.Forms.GroupBox();
            this.pressText = new System.Windows.Forms.Label();
            this.oldGroup.SuspendLayout();
            this.newGroup.SuspendLayout();
            this.bindGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // oldGroup
            // 
            this.oldGroup.Controls.Add(this.oldCharText);
            this.oldGroup.Controls.Add(this.oldCodeText);
            this.oldGroup.Controls.Add(this.oCharText);
            this.oldGroup.Controls.Add(this.oCodeText);
            this.oldGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.oldGroup.Location = new System.Drawing.Point(151, 54);
            this.oldGroup.Name = "oldGroup";
            this.oldGroup.Size = new System.Drawing.Size(136, 65);
            this.oldGroup.TabIndex = 16;
            this.oldGroup.TabStop = false;
            this.oldGroup.Text = "Old key bind";
            // 
            // oldCharText
            // 
            this.oldCharText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.oldCharText.Location = new System.Drawing.Point(6, 16);
            this.oldCharText.Name = "oldCharText";
            this.oldCharText.Size = new System.Drawing.Size(68, 19);
            this.oldCharText.TabIndex = 6;
            this.oldCharText.Text = "Key Char:";
            this.oldCharText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.oldCharText.UseCompatibleTextRendering = true;
            // 
            // oldCodeText
            // 
            this.oldCodeText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.oldCodeText.Location = new System.Drawing.Point(6, 37);
            this.oldCodeText.Name = "oldCodeText";
            this.oldCodeText.Size = new System.Drawing.Size(68, 19);
            this.oldCodeText.TabIndex = 8;
            this.oldCodeText.Text = "Key Code:";
            this.oldCodeText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.oldCodeText.UseCompatibleTextRendering = true;
            // 
            // oCharText
            // 
            this.oCharText.AutoEllipsis = true;
            this.oCharText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.oCharText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.oCharText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.oCharText.Location = new System.Drawing.Point(80, 16);
            this.oCharText.Name = "oCharText";
            this.oCharText.Size = new System.Drawing.Size(50, 19);
            this.oCharText.TabIndex = 3;
            this.oCharText.Text = "CHAR";
            this.oCharText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.oCharText.UseCompatibleTextRendering = true;
            // 
            // oCodeText
            // 
            this.oCodeText.AutoEllipsis = true;
            this.oCodeText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.oCodeText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.oCodeText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.oCodeText.Location = new System.Drawing.Point(80, 37);
            this.oCodeText.Name = "oCodeText";
            this.oCodeText.Size = new System.Drawing.Size(50, 19);
            this.oCodeText.TabIndex = 4;
            this.oCodeText.Text = "CODE";
            this.oCodeText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.oCodeText.UseCompatibleTextRendering = true;
            // 
            // saveButton
            // 
            this.saveButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.saveButton.Location = new System.Drawing.Point(234, 123);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(53, 21);
            this.saveButton.TabIndex = 17;
            this.saveButton.TabStop = false;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            // 
            // newGroup
            // 
            this.newGroup.Controls.Add(this.newCodeText);
            this.newGroup.Controls.Add(this.newCharText);
            this.newGroup.Controls.Add(this.nCodeText);
            this.newGroup.Controls.Add(this.nCharText);
            this.newGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newGroup.Location = new System.Drawing.Point(9, 54);
            this.newGroup.Name = "newGroup";
            this.newGroup.Size = new System.Drawing.Size(136, 65);
            this.newGroup.TabIndex = 16;
            this.newGroup.TabStop = false;
            this.newGroup.Text = "New key bind";
            this.newGroup.UseCompatibleTextRendering = true;
            // 
            // newCodeText
            // 
            this.newCodeText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.newCodeText.Location = new System.Drawing.Point(6, 37);
            this.newCodeText.Name = "newCodeText";
            this.newCodeText.Size = new System.Drawing.Size(68, 19);
            this.newCodeText.TabIndex = 7;
            this.newCodeText.Text = "Key Code:";
            this.newCodeText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.newCodeText.UseCompatibleTextRendering = true;
            // 
            // newCharText
            // 
            this.newCharText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.newCharText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.newCharText.Location = new System.Drawing.Point(6, 16);
            this.newCharText.Name = "newCharText";
            this.newCharText.Size = new System.Drawing.Size(68, 19);
            this.newCharText.TabIndex = 5;
            this.newCharText.Text = "Key Char:";
            this.newCharText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.newCharText.UseCompatibleTextRendering = true;
            // 
            // nCodeText
            // 
            this.nCodeText.AutoEllipsis = true;
            this.nCodeText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.nCodeText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.nCodeText.ForeColor = System.Drawing.Color.Red;
            this.nCodeText.Location = new System.Drawing.Point(80, 37);
            this.nCodeText.Name = "nCodeText";
            this.nCodeText.Size = new System.Drawing.Size(50, 19);
            this.nCodeText.TabIndex = 2;
            this.nCodeText.Text = "CODE";
            this.nCodeText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.nCodeText.UseCompatibleTextRendering = true;
            // 
            // nCharText
            // 
            this.nCharText.AutoEllipsis = true;
            this.nCharText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.nCharText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.nCharText.ForeColor = System.Drawing.Color.Red;
            this.nCharText.Location = new System.Drawing.Point(80, 16);
            this.nCharText.Name = "nCharText";
            this.nCharText.Size = new System.Drawing.Size(50, 19);
            this.nCharText.TabIndex = 1;
            this.nCharText.Text = "CHAR";
            this.nCharText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.nCharText.UseCompatibleTextRendering = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cancelButton.Location = new System.Drawing.Point(175, 123);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(53, 21);
            this.cancelButton.TabIndex = 18;
            this.cancelButton.TabStop = false;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // bindGroup
            // 
            this.bindGroup.Controls.Add(this.pressText);
            this.bindGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bindGroup.Location = new System.Drawing.Point(9, 2);
            this.bindGroup.Name = "bindGroup";
            this.bindGroup.Size = new System.Drawing.Size(278, 46);
            this.bindGroup.TabIndex = 15;
            this.bindGroup.TabStop = false;
            this.bindGroup.Text = "Bind key";
            // 
            // pressText
            // 
            this.pressText.AutoSize = true;
            this.pressText.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.pressText.Location = new System.Drawing.Point(42, 16);
            this.pressText.Name = "pressText";
            this.pressText.Size = new System.Drawing.Size(196, 22);
            this.pressText.TabIndex = 0;
            this.pressText.Text = "Please press any key...";
            // 
            // bindForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 149);
            this.Controls.Add(this.bindGroup);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.newGroup);
            this.Controls.Add(this.oldGroup);
            this.Controls.Add(this.saveButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "bindForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bind key";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.bindForm_Load);
            this.oldGroup.ResumeLayout(false);
            this.newGroup.ResumeLayout(false);
            this.bindGroup.ResumeLayout(false);
            this.bindGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox oldGroup;
        private System.Windows.Forms.Label oldCharText;
        private System.Windows.Forms.Label oldCodeText;
        private System.Windows.Forms.Label oCharText;
        private System.Windows.Forms.Label oCodeText;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.GroupBox newGroup;
        private System.Windows.Forms.Label newCodeText;
        private System.Windows.Forms.Label nCodeText;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.GroupBox bindGroup;
        private System.Windows.Forms.Label pressText;
        private System.Windows.Forms.Label newCharText;
        private System.Windows.Forms.Label nCharText;
    }
}
﻿namespace NET_Launcher.Forms
{
    partial class ContentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.contentBox = new System.Windows.Forms.GroupBox();
            this.editorButton = new System.Windows.Forms.Button();
            this.stageButton = new System.Windows.Forms.Button();
            this.charButton = new System.Windows.Forms.Button();
            this.gameButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.contentBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // contentBox
            // 
            this.contentBox.Controls.Add(this.editorButton);
            this.contentBox.Controls.Add(this.stageButton);
            this.contentBox.Controls.Add(this.charButton);
            this.contentBox.Controls.Add(this.gameButton);
            this.contentBox.Location = new System.Drawing.Point(7, 12);
            this.contentBox.Name = "contentBox";
            this.contentBox.Size = new System.Drawing.Size(376, 128);
            this.contentBox.TabIndex = 0;
            this.contentBox.TabStop = false;
            this.contentBox.Text = "Content Box";
            // 
            // editorButton
            // 
            this.editorButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.editorButton.Enabled = false;
            this.editorButton.FlatAppearance.BorderSize = 0;
            this.editorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editorButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F);
            this.editorButton.Location = new System.Drawing.Point(274, 19);
            this.editorButton.Name = "editorButton";
            this.editorButton.Size = new System.Drawing.Size(79, 92);
            this.editorButton.TabIndex = 3;
            this.editorButton.Text = "Char editor";
            this.editorButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.editorButton.UseCompatibleTextRendering = true;
            this.editorButton.UseVisualStyleBackColor = true;
            this.editorButton.Click += new System.EventHandler(this.editorButton_Click);
            // 
            // stageButton
            // 
            this.stageButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.stageButton.Enabled = false;
            this.stageButton.FlatAppearance.BorderSize = 0;
            this.stageButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stageButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F);
            this.stageButton.Location = new System.Drawing.Point(189, 19);
            this.stageButton.Name = "stageButton";
            this.stageButton.Size = new System.Drawing.Size(79, 92);
            this.stageButton.TabIndex = 2;
            this.stageButton.Text = "Stage Manager";
            this.stageButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.stageButton.UseCompatibleTextRendering = true;
            this.stageButton.UseVisualStyleBackColor = true;
            // 
            // charButton
            // 
            this.charButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.charButton.Enabled = false;
            this.charButton.FlatAppearance.BorderSize = 0;
            this.charButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.charButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F);
            this.charButton.Location = new System.Drawing.Point(104, 19);
            this.charButton.Name = "charButton";
            this.charButton.Size = new System.Drawing.Size(79, 92);
            this.charButton.TabIndex = 1;
            this.charButton.Text = "Char download";
            this.charButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.charButton.UseCompatibleTextRendering = true;
            this.charButton.UseVisualStyleBackColor = true;
            // 
            // gameButton
            // 
            this.gameButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gameButton.FlatAppearance.BorderSize = 0;
            this.gameButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gameButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F);
            this.gameButton.Location = new System.Drawing.Point(19, 19);
            this.gameButton.Name = "gameButton";
            this.gameButton.Size = new System.Drawing.Size(79, 92);
            this.gameButton.TabIndex = 0;
            this.gameButton.Text = "Game Download";
            this.gameButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.gameButton.UseCompatibleTextRendering = true;
            this.gameButton.UseVisualStyleBackColor = true;
            this.gameButton.Click += new System.EventHandler(this.gameButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.exitButton.Location = new System.Drawing.Point(308, 146);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 23);
            this.exitButton.TabIndex = 1;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            // 
            // ContentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.exitButton;
            this.ClientSize = new System.Drawing.Size(395, 174);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.contentBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ContentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Game Content";
            this.Load += new System.EventHandler(this.ContentForm_Load);
            this.contentBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox contentBox;
        private System.Windows.Forms.Button charButton;
        private System.Windows.Forms.Button gameButton;
        private System.Windows.Forms.Button editorButton;
        private System.Windows.Forms.Button stageButton;
        private System.Windows.Forms.Button exitButton;
    }
}
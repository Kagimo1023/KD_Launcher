﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using NET_Launcher.Classes;

namespace NET_Launcher.Forms
{
    public partial class EditKeys : Form
    {
        bindForm bindForm;

        Vars publicVar = new Vars();
        Methods publicMethod = new Methods();
        INIManager manager = new INIManager(Vars.cfgPath);

        static int buttonNumber;

        int initDevice;
        int initResult;
        int initValue;

        Object selectPlayer;
        Object selectDevice;

        public static Dictionary<int, string> keysDictionary = new Dictionary<int, string>(10);
        List<Button> buttons = new List<Button>();
        List<EventHandler> events = new List<EventHandler>();
        List<string> keys = new List<string> { "Up", "Left", "Down", "Right",
                                               "Trigger1", "Trigger2", "Trigger3", "Trigger4", "Trigger5",
                                               "Select", "Start" };

        public static Button temp;

        public EditKeys()
        {
            InitializeComponent();

            buttons.Add(button1); events.Add(button1_Click);
            buttons.Add(button2); events.Add(button2_Click);
            buttons.Add(button3); events.Add(button3_Click);
            buttons.Add(button4); events.Add(button4_Click);
            buttons.Add(button5); events.Add(button5_Click);
            buttons.Add(button6); events.Add(button6_Click);
            buttons.Add(button7); events.Add(button7_Click);
            buttons.Add(button8); events.Add(button8_Click);
            buttons.Add(button9); events.Add(button9_Click);
            buttons.Add(button10); events.Add(button10_Click);
            buttons.Add(button11); events.Add(button11_Click);

            playerList.SetSelected(1, true);

            selectPlayer = playerList.SelectedIndex;

            initDevice = Convert.ToInt32(manager.GetPrivateString("InitDevice", selectPlayer.ToString() + "P"));

            if (initDevice == 0) initResult = 0;
            if (initDevice == -1) initResult = 1;

            deviceSelect.SelectedIndex = initResult;

            selectDevice = deviceSelect.SelectedItem;

            Vars.resultString = selectDevice.ToString() + selectPlayer.ToString() + "P";

            buttonText();

            for (int i = 1; i <= 10; i++) buttons[i].Click += new EventHandler(events[i]);

            playerList.SelectedIndexChanged += new EventHandler(playerList_SelectedIndexChanged);
            deviceSelect.SelectedIndexChanged += new EventHandler(deviceSelect_SelectedIndexChanged);
        }

        private void buttonText()
        {
            for (int i = 0; i <= 10; i++)
            {
                buttons[i].Text = manager.GetPrivateString(Vars.resultString, keys[i]);
                Vars.Kdata = buttons[i].Text;
                keysDictionary[i] = Vars.Kdata;
            }
            Update();
        }
        
        static void buttonUpdate()
        {
            keysDictionary[buttonNumber] = Vars.KeyData;
            temp.Text = Vars.KeyData;
            temp.Update();
        }

        private void deviceSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectDevice = deviceSelect.SelectedItem;

            if (deviceSelect.SelectedIndex == 0) initValue = 0; if (deviceSelect.SelectedIndex == 1) initValue = -1;

            Vars.resultString = selectDevice.ToString() + selectPlayer.ToString() + "P";

            buttonText();
            saveButton.Enabled = true;
        }

        private void playerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectPlayer = playerList.SelectedIndex;

            if (playerList.SelectedIndex == 0) playerList.SelectedIndex = 1;
            initDevice = Convert.ToInt32(manager.GetPrivateString("InitDevice", selectPlayer.ToString() + "P"));

            if (initDevice == 0) initResult = 0; if (initDevice == -1) initResult = 1;

            deviceSelect.SelectedIndex = initResult;

            Vars.resultString = selectDevice.ToString() + selectPlayer.ToString() + "P";

            buttonText();
            saveButton.Enabled = true;
        }

        public static void bindForm_Closed(object sender, EventArgs e)
        {
            buttonUpdate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buttonNumber = 0;
            Vars.Key = "Up";
            temp = (Button)sender;
            bindForm = new bindForm("Button Up");
            bindForm.ShowDialog();
            saveButton.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            buttonNumber = 1;
            Vars.Key = "Left";
            temp = (Button)sender;
            bindForm = new bindForm("Button Left");
            bindForm.ShowDialog();
            saveButton.Enabled = true;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            buttonNumber = 2;
            Vars.Key = "Down";
            temp = (Button)sender;
            bindForm = new bindForm("Button Down");
            bindForm.ShowDialog();
            saveButton.Enabled = true;
        }
        private void button4_Click(object sender, EventArgs e)
        {
            buttonNumber = 3;
            Vars.Key = "Right";
            temp = (Button)sender;
            bindForm = new bindForm("Button Right");
            bindForm.ShowDialog();
            saveButton.Enabled = true;
        }
        private void button5_Click(object sender, EventArgs e)
        {
            buttonNumber = 4;
            Vars.Key = "Trigger1";
            temp = (Button)sender;
            bindForm = new bindForm("Button A");
            bindForm.ShowDialog();
            saveButton.Enabled = true;
        }
        private void button6_Click(object sender, EventArgs e)
        {
            buttonNumber = 5;
            Vars.Key = "Trigger2";
            temp = (Button)sender;
            bindForm = new bindForm("Button B");
            bindForm.ShowDialog();
            saveButton.Enabled = true;
        }
        private void button7_Click(object sender, EventArgs e)
        {
            buttonNumber = 6;
            Vars.Key = "Trigger3";
            temp = (Button)sender;
            bindForm = new bindForm("Button C");
            bindForm.ShowDialog();
            saveButton.Enabled = true;
        }
        private void button8_Click(object sender, EventArgs e)
        {
            buttonNumber = 7;
            Vars.Key = "Trigger4";
            temp = (Button)sender;
            bindForm = new bindForm("Button A+B");
            bindForm.ShowDialog();
            saveButton.Enabled = true;
        }
        private void button9_Click(object sender, EventArgs e)
        {
            buttonNumber = 8;
            Vars.Key = "Trigger5";
            temp = (Button)sender;
            bindForm = new bindForm("Button A+C");
            bindForm.ShowDialog();
            saveButton.Enabled = true;
        }
        private void button10_Click(object sender, EventArgs e)
        {
            buttonNumber = 9;
            Vars.Key = "Select";
            temp = (Button)sender;
            bindForm = new bindForm("Button Select");
            bindForm.ShowDialog();
            saveButton.Enabled = true;
        }
        private void button11_Click(object sender, EventArgs e)
        {
            buttonNumber = 10;
            Vars.Key = "Start";
            temp = (Button)sender;
            bindForm = new bindForm("Button Start");
            bindForm.ShowDialog();
            saveButton.Enabled = true;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i <= 10; i++)
            {
                manager.WritePrivateString(Vars.resultString, keys[i], keysDictionary[i]);
            }

            manager.WritePrivateString("InitDevice", selectPlayer.ToString() + "P", initValue.ToString());
            saveButton.Enabled = false;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

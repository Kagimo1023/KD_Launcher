﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using NET_Launcher.Classes;
using System.Windows.Forms;

namespace NET_Launcher.Forms
{
    public partial class CharEditor : Form
    {
        //Проверка состояния буфера
        bool bufferEmpty = true;

        //Переменные контролов
        private int selectID;
        private bool foundBool;

        private List<string> spcAbout1 = new List<string>();
        private List<string> spcAbout2 = new List<string>();
        private List<string> spcAbout3 = new List<string>();

        //Инициализация bmp файлов
        private static Bitmap faceBmp;
        private static Bitmap bodyBmp;
        private static Bitmap headBmp;

        //Moveset data
        private List<int> moveNumb = new List<int>();
        private List<string> moveName = new List<string>();
        private List<string> moveAbout = new List<string>();
        private List<string> moveEffect = new List<string>();
        private List<string> moveInfluence = new List<string>();

        //Инициализация персонажей
        private List<string> nameData = new List<string>();
        private List<int> faceData = new List<int>();
        private List<int> headData = new List<int>();
        private List<int> skinData = new List<int>();
        private List<int> bodyData = new List<int>();
        private List<int> healData = new List<int>();
        private List<int> fistData = new List<int>();
        private List<int> kickData = new List<int>();
        private List<int> tossData = new List<int>();
        private List<int> weapData = new List<int>();
        private List<int> weaTData = new List<int>();
        private List<int> jumpData = new List<int>();
        private List<int> agilData = new List<int>();
        private List<int> reveData = new List<int>();
        private List<int> defeData = new List<int>();

        private List<int> spc1Data = new List<int>();
        private List<int> spc2Data = new List<int>();
        private List<int> spc3Data = new List<int>();

        //Буфер данных
        private List<string> bufferName = new List<string>();
        private List<int> bufferFace = new List<int>();
        private List<int> bufferHead = new List<int>();
        private List<int> bufferSkin = new List<int>();
        private List<int> bufferBody = new List<int>();
        private List<int> bufferHeal = new List<int>();
        private List<int> bufferFist = new List<int>();
        private List<int> bufferKick = new List<int>();
        private List<int> bufferToss = new List<int>();
        private List<int> bufferWeap = new List<int>();
        private List<int> bufferWeaT = new List<int>();
        private List<int> bufferJump = new List<int>();
        private List<int> bufferAgil = new List<int>();
        private List<int> bufferReve = new List<int>();
        private List<int> bufferDefe = new List<int>();

        private List<int> bufferSp1 = new List<int>();
        private List<int> bufferSp2 = new List<int>();
        private List<int> bufferSp3 = new List<int>();

        private List<string> bufferList = new List<string>();

        private string moveData;
        private string fileData;
        private string fileSkinData;

        private IEnumerable<int> rangeNothing = Enumerable.Range(141, 254);
        private IEnumerable<int> rangePassive = Enumerable.Range(73, 130);
        private IEnumerable<int> rangeAny = Enumerable.Range(1, 255);

        bool TestRange(int numberToCheck, int bottom, int top)
        {
            return (numberToCheck >= bottom && numberToCheck <= top);
        }

        //Палитры / Цвета
        private List<Color> group = new List<Color>();
        private List<int> red = new List<int>();
        private List<int> green = new List<int>();
        private List<int> blue = new List<int>();

        private List<int> groupBorder = new List<int>();
        private List<int> redBorder = new List<int>();
        private List<int> greenBorder = new List<int>();
        private List<int> blueBorder = new List<int>();

        private List<Color> bufferGroup = new List<Color>();
        private List<int> bufferRed = new List<int>();
        private List<int> bufferGreen = new List<int>();
        private List<int> bufferBlue = new List<int>();

        private List<int> bufferGroupB = new List<int>();
        private List<int> bufferRedB = new List<int>();
        private List<int> bufferGreenB = new List<int>();
        private List<int> bufferBlueB = new List<int>();

        //Стандартные сетки для bmp
        private Rectangle rectangleH = new Rectangle(32, 0, 16, 8);  //Сетка для голов
        private Rectangle rectangleF = new Rectangle(0, 0, 14, 8);  //Сетка для лиц
        private Rectangle rectangleB = new Rectangle(0, 0, 24, 24); //Сетка для тел
        private Rectangle rectangleC = new Rectangle(72, 40, 24, 24);

        public CharEditor()
        {
            //Инициализация формы
            InitializeComponent();

            //Асихронные события
            charList.DrawMode = DrawMode.OwnerDrawFixed;
            borderColorButton.BackColorChanged += new EventHandler(borderColorButton_BackColorChanged);
            skinColorButton.BackColorChanged += new EventHandler(skinColorButton_BackColorChanged);
            charList.DrawItem += new DrawItemEventHandler(charList_DrawItem);
            nameBox.LostFocus += new EventHandler(nameBox_LostFocus);
            charList.KeyDown += new KeyEventHandler(charList_KeyDown);

            specialNumeric1.SelectedValueChanged += new EventHandler(specialNumeric1_SelectedValueChanged);
            specialNumeric2.SelectedValueChanged += new EventHandler(specialNumeric1_SelectedValueChanged);
            specialNumeric3.SelectedValueChanged += new EventHandler(specialNumeric1_SelectedValueChanged);

            fileListLoad();                  //Инициализация файлов
            sectorsInitiliaze();             //Инициализация параметров
            pictureInitialize();             //Инициализация картинок
            NumericInit();                   //Инициализация numericDownUp
        }

        private void sectorsInitiliaze()
        {
            foundBool = false;
            object selected = charList.SelectedItem;

            if (bufferEmpty == true)
            {
                bufferName = nameData;
                bufferFace = faceData;
                bufferHead = headData;
                bufferSkin = skinData;
                bufferBody = bodyData;
                bufferHeal = healData;
                bufferFist = fistData;
                bufferKick = kickData;
                bufferToss = tossData;
                bufferWeap = weapData;
                bufferWeaT = weaTData;
                bufferAgil = agilData;
                bufferJump = jumpData;
                bufferReve = reveData;
                bufferDefe = defeData;

                bufferSp1 = spc1Data;
                bufferSp2 = spc2Data;
                bufferSp3 = spc3Data;

                bufferRed = red;
                bufferGreen = green;
                bufferBlue = blue;

                bufferRedB = redBorder;
                bufferBlueB = blueBorder;
                bufferGreenB = greenBorder;

                bufferEmpty = false;
            }

            if (bufferEmpty == false)
            {
                if (selected != null)
                {
                    nameBox.Text = bufferName[selectID];
                    faceNumeric.Value = bufferFace[selectID];
                    headNumeric.Value = bufferHead[selectID];
                    skinNumeric.Value = bufferSkin[selectID];
                    bodyNumeric.Value = bufferBody[selectID];
                    healthNumeric.Value = bufferHeal[selectID];
                    punchNumeric.Value = bufferFist[selectID];
                    kickNumeric.Value = bufferKick[selectID];
                    tossNumeric.Value = bufferToss[selectID];
                    weaponNumeric.Value = bufferWeap[selectID];
                    weaponTNumeric.Value = bufferWeaT[selectID];
                    agilityNumeric.Value = bufferAgil[selectID];
                    jumpNumeric.Value = bufferJump[selectID];
                    reverseNumeric.Value = bufferReve[selectID];
                    defenseNumeric.Value = bufferDefe[selectID];
                    checkSpecial();

                    buttonColorInitialize();
                }
            }
        }

        private void checkSpecial()
        {
            if (specialNumeric1.Items.Count > bufferSp1[selectID]) specialNumeric1.SelectedIndex = bufferSp1[selectID];
            if (specialNumeric2.Items.Count > bufferSp2[selectID]) specialNumeric2.SelectedIndex = bufferSp2[selectID];
            if (specialNumeric3.Items.Count > bufferSp3[selectID]) specialNumeric3.SelectedIndex = bufferSp3[selectID];

            if (bufferSp1[selectID] == 130) { specialNumeric1.SelectedIndex = 73; }
            if (bufferSp1[selectID] == 131) { specialNumeric1.SelectedIndex = 74; }
            if (bufferSp1[selectID] == 132) { specialNumeric1.SelectedIndex = 75; }
            if (bufferSp1[selectID] == 133) { specialNumeric1.SelectedIndex = 76; }
            if (bufferSp1[selectID] == 134) { specialNumeric1.SelectedIndex = 77; }
            if (bufferSp1[selectID] == 135) { specialNumeric1.SelectedIndex = 78; }
            if (bufferSp1[selectID] == 136) { specialNumeric1.SelectedIndex = 79; }
            if (bufferSp1[selectID] == 137) { specialNumeric1.SelectedIndex = 80; }
            if (bufferSp1[selectID] == 138) { specialNumeric1.SelectedIndex = 81; }
            if (bufferSp1[selectID] == 140) { specialNumeric1.SelectedIndex = 82; }

            if (bufferSp2[selectID] == 130) { specialNumeric2.SelectedIndex = 73; }
            if (bufferSp2[selectID] == 131) { specialNumeric2.SelectedIndex = 74; }
            if (bufferSp2[selectID] == 132) { specialNumeric2.SelectedIndex = 75; }
            if (bufferSp2[selectID] == 133) { specialNumeric2.SelectedIndex = 76; }
            if (bufferSp2[selectID] == 134) { specialNumeric2.SelectedIndex = 77; }
            if (bufferSp2[selectID] == 135) { specialNumeric2.SelectedIndex = 78; }
            if (bufferSp2[selectID] == 136) { specialNumeric2.SelectedIndex = 79; }
            if (bufferSp2[selectID] == 137) { specialNumeric2.SelectedIndex = 80; }
            if (bufferSp2[selectID] == 138) { specialNumeric2.SelectedIndex = 81; }
            if (bufferSp2[selectID] == 140) { specialNumeric2.SelectedIndex = 82; }

            if (bufferSp3[selectID] == 130) { specialNumeric3.SelectedIndex = 73; }
            if (bufferSp3[selectID] == 131) { specialNumeric3.SelectedIndex = 74; }
            if (bufferSp3[selectID] == 132) { specialNumeric3.SelectedIndex = 75; }
            if (bufferSp3[selectID] == 133) { specialNumeric3.SelectedIndex = 76; }
            if (bufferSp3[selectID] == 134) { specialNumeric3.SelectedIndex = 77; }
            if (bufferSp3[selectID] == 135) { specialNumeric3.SelectedIndex = 78; }
            if (bufferSp3[selectID] == 136) { specialNumeric3.SelectedIndex = 79; }
            if (bufferSp3[selectID] == 137) { specialNumeric3.SelectedIndex = 80; }
            if (bufferSp3[selectID] == 138) { specialNumeric3.SelectedIndex = 81; }
            if (bufferSp3[selectID] == 140) { specialNumeric3.SelectedIndex = 82; }

            if (bufferSp1[selectID] == 255) { specialNumeric1.SelectedIndex = 83; }
            if (bufferSp2[selectID] == 255) { specialNumeric2.SelectedIndex = 83; }
            if (bufferSp3[selectID] == 255) { specialNumeric3.SelectedIndex = 83; }
        }

        private void charList_KeyDown(object sender, KeyEventArgs e)
        {
            if (charList.Items.Count > 1)
            {
                if (e.KeyCode == Keys.Delete)
                {
                    bufferName.Remove(bufferName[selectID]);
                    bufferFace.Remove(bufferFace[selectID]);
                    bufferBody.Remove(bufferBody[selectID]);
                    bufferHead.Remove(bufferHead[selectID]);
                    bufferSkin.Remove(bufferSkin[selectID]);
                    bufferHeal.Remove(bufferHeal[selectID]);
                    bufferFist.Remove(bufferFist[selectID]);
                    bufferKick.Remove(bufferKick[selectID]);
                    bufferToss.Remove(bufferToss[selectID]);
                    bufferWeap.Remove(bufferWeap[selectID]);
                    bufferWeaT.Remove(bufferWeaT[selectID]);
                    bufferAgil.Remove(bufferAgil[selectID]);
                    bufferJump.Remove(bufferJump[selectID]);
                    bufferReve.Remove(bufferReve[selectID]);
                    bufferDefe.Remove(bufferDefe[selectID]);

                    bufferSp1.Remove(bufferSp1[selectID]);
                    bufferSp2.Remove(bufferSp2[selectID]);
                    bufferSp3.Remove(bufferSp3[selectID]);

                    charList.Items.Remove(charList.Items[selectID]);
                    charList.SelectedIndex = 0;
                }
                label20.Text = charList.Items.Count.ToString();
            }
        }

        private void nameBox_LostFocus(object sender, EventArgs e)
        {
            charList.Items[selectID] = bufferName[selectID];
        }

        private void skinColorButton_BackColorChanged(object sender, EventArgs e)
        {
        }

        private void borderColorButton_BackColorChanged(object sender, EventArgs e)
        {
        }

        private void buttonColorInitialize()
        {
            borderColorButton.BackColor = Color.FromArgb(bufferRedB[bufferSkin[selectID]], bufferBlueB[bufferSkin[selectID]], bufferGreenB[bufferSkin[selectID]]);
            skinColorButton.BackColor = Color.FromArgb(bufferRed[bufferSkin[selectID]], bufferBlue[bufferSkin[selectID]], bufferGreen[bufferSkin[selectID]]);
        }

        private void pictureInitialize()
        {
            facePicture.Image = Image.FromFile(@"Graphic\Face.bmp");
            bodyPicture.Image = Image.FromFile(@"Graphic\CharSelBody.bmp");
            headPicture.Image = Image.FromFile(@"Graphic\Head.bmp");

            faceBmp = (Bitmap)facePicture.Image;
            bodyBmp = (Bitmap)bodyPicture.Image;
            headBmp = (Bitmap)headPicture.Image;

            facePicture.Image = ResizeImage(Copy(faceBmp, rectangleF), 42, 24);
            bodyPicture.Image = ResizeImage(Copy(bodyBmp, rectangleB), 72, 72);
        }

        private void fileListLoad()
        {
            //Moveset data load
            try
            {
                using (StreamReader sr = new StreamReader("moveset.txt"))
                {
                    String lines = sr.ReadToEnd();
                    moveData = SubstringExtensions.Between(lines, "#START", "#END");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                var options = RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.Compiled;
                string pattern = @"^\u007c\s*(\d*)\s*\u007c\s*(\S*\w*\s*\w*\s*\w*)\s*\u007c\s*";
                var matches = Regex.Matches(moveData, pattern, options);
                foreach (Match match in matches)
                {
                    moveNumb.Add(Convert.ToInt32(match.Groups[1].Value));
                    moveName.Add(match.Groups[2].Value);
                    moveAbout.Add(match.Groups[3].Value);
                    //moveEffect.Add(match.Groups[4].Value);
                    //moveInfluence.Add(match.Groups[5].Value);
                }
                for (int i = 0; i < moveName.Count; i++)
                {
                    specialNumeric1.Items.Add(moveName[i]);
                    specialNumeric2.Items.Add(moveName[i]);
                    specialNumeric3.Items.Add(moveName[i]);

                    specialNumeric1.DisplayMember = moveName[i];
                    specialNumeric2.DisplayMember = moveName[i];
                    specialNumeric3.DisplayMember = moveName[i];
                }
                for (int i = 0; i < moveNumb.Count; i++)
                {
                    specialNumeric1.ValueMember = moveNumb[i].ToString();
                    specialNumeric2.ValueMember = moveNumb[i].ToString();
                    specialNumeric3.ValueMember = moveNumb[i].ToString();
                }
            }

            //Skin color data load
            try
            {
                using (StreamReader sr = new StreamReader(@"Graphic\SkinColor.txt", Encoding.GetEncoding(932)))
                {
                    String lines = sr.ReadToEnd();
                    fileSkinData = SubstringExtensions.Between(lines, "#START", "#END");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                var options = RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.Compiled;
                string pattern = @"^(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*)";

                var matches = Regex.Matches(fileSkinData, pattern, options);
                foreach (Match match in matches)
                {
                    //groupBorder.Add(Convert.ToInt32(match.Groups[3]));
                    redBorder.Add(Convert.ToInt32(match.Groups[4].Value));
                    blueBorder.Add(Convert.ToInt32(match.Groups[5].Value));
                    greenBorder.Add(Convert.ToInt32(match.Groups[6].Value));

                    //group.Add(Convert.ToInt32(match.Groups[7]));
                    red.Add(Convert.ToInt32(match.Groups[8].Value));
                    blue.Add(Convert.ToInt32(match.Groups[9].Value));
                    green.Add(Convert.ToInt32(match.Groups[10].Value));

                    group.Add(Color.FromArgb(Convert.ToInt32(match.Groups[8].Value), Convert.ToInt32(match.Groups[10].Value), Convert.ToInt32(match.Groups[9].Value)));
                    skinNumeric.Maximum = matches.Count;
                }
                --skinNumeric.Maximum;
            }

            //Character data load
            try
            {
                using (StreamReader sr = new StreamReader("CharData.txt", Encoding.GetEncoding(932)))
                {
                    String lines = sr.ReadToEnd();
                    fileData = SubstringExtensions.Between(lines, "#START", "#END");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                var options = RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.Compiled;
                string pattern = @"^(\w*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*), *(\d*)";

                var matches = Regex.Matches(fileData, pattern, options);
                foreach (Match match in matches)
                {
                    nameData.Add(match.Groups[1].Value);
                    faceData.Add(Convert.ToInt32(match.Groups[2].Value));
                    headData.Add(Convert.ToInt32(match.Groups[3].Value));
                    skinData.Add(Convert.ToInt32(match.Groups[4].Value));
                    bodyData.Add(Convert.ToInt32(match.Groups[5].Value));
                    healData.Add(Convert.ToInt32(match.Groups[6].Value));
                    fistData.Add(Convert.ToInt32(match.Groups[7].Value));
                    kickData.Add(Convert.ToInt32(match.Groups[8].Value));
                    tossData.Add(Convert.ToInt32(match.Groups[9].Value));
                    weapData.Add(Convert.ToInt32(match.Groups[10].Value));
                    weaTData.Add(Convert.ToInt32(match.Groups[11].Value));
                    agilData.Add(Convert.ToInt32(match.Groups[12].Value));
                    jumpData.Add(Convert.ToInt32(match.Groups[13].Value));
                    reveData.Add(Convert.ToInt32(match.Groups[14].Value));
                    defeData.Add(Convert.ToInt32(match.Groups[15].Value));
                    spc1Data.Add(Convert.ToInt32(match.Groups[16].Value));
                    spc2Data.Add(Convert.ToInt32(match.Groups[17].Value));
                    spc3Data.Add(Convert.ToInt32(match.Groups[18].Value));
                }
                for (int i = 0; i < nameData.Count; i++) charList.Items.Add(nameData[i]);
                charList.SelectedIndex = 0;
            }
        }

        private void skinNumeric_ValueChanged(object sender, EventArgs e)
        {
            int selectColor = (int)skinNumeric.Value;
            if (selectColor <= skinNumeric.Maximum) borderColorButton.BackColor = Color.FromArgb(bufferRedB[selectColor], bufferBlueB[selectColor], bufferGreenB[selectColor]);
            if (selectColor <= skinNumeric.Maximum) skinColorButton.BackColor = Color.FromArgb(bufferRed[selectColor], bufferBlue[selectColor], bufferGreen[selectColor]);
            bufferSkin[selectID] = selectColor;
        }

        private void NumericInit()
        {
            label20.Text = charList.Items.Count.ToString();

            for (int i = 0; i < faceBmp.Height; i += 8) { ++faceNumeric.Maximum; }
            --faceNumeric.Maximum;

            for (int i = 0; i < bodyBmp.Width; i += 24) { ++bodyNumeric.Maximum; }
            --bodyNumeric.Maximum;

            for (int i = 0; i < headBmp.Height; i += 8) { ++headNumeric.Maximum; }
            --headNumeric.Maximum;
        }

        private void charList_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectID = charList.SelectedIndex;
            //charList.Items[selectID] = bufferName[selectID];
            sectorsInitiliaze();
        }

        private void headNumeric_ValueChanged(object sender, EventArgs e)
        {
            bufferHead[selectID] = (int)headNumeric.Value;
        }

        private void faceNumeric_ValueChangedUpDown(object sender, EventArgs e)
        {
            //Создание сетки для метода Copy
            int hyperValue = (int)faceNumeric.Value * 8;
            Rectangle rectangle = new Rectangle(0, hyperValue, 14, 8);
            facePicture.Image = ResizeImage(Copy(faceBmp, rectangle), 42, 24); //Вставка вырезаного куска изображения в PictureBox
            bufferFace[selectID] = (int)faceNumeric.Value;
        }

        private void bodyNumeric_ValueChangedUpDown(object sender, EventArgs e)
        {
            //Создание сетки для метода Copy
            int hyperValue = (int)bodyNumeric.Value * 24;
            Rectangle rectangle = new Rectangle(hyperValue, 0, 24, 24);

            //Вставка вырезаного куска изображения в PictureBox
            bodyPicture.Image = ResizeImage(Copy(bodyBmp, rectangle), 72, 72);
            bufferBody[selectID] = (int)bodyNumeric.Value;
        }

        public static Image Copy(Image srcBitmap, Rectangle section)
        {
            // Вырезаем выбранный кусок картинки
            Bitmap bmp = new Bitmap(section.Width, section.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                g.DrawImage(srcBitmap, 0, 0, section, GraphicsUnit.Pixel);
            }
            //Возвращаем кусок картинки.
            return bmp;
        }

        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.Default;
                graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
                graphics.SmoothingMode = SmoothingMode.Default;
                graphics.PixelOffsetMode = PixelOffsetMode.Default;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }
            return destImage;
        }

        private void borderColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                borderColorButton.BackColor = colorDialog1.Color;
            }
        }

        private void skinColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog2.ShowDialog() == DialogResult.OK)
            {
                skinColorButton.BackColor = colorDialog2.Color;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random faceRandom = new Random();
            Random bodyRandom = new Random();
            Random statRandom = new Random();
            Random healRandom = new Random();

            bufferFace[selectID] = faceRandom.Next(0, (int)faceNumeric.Maximum);
            bufferBody[selectID] = bodyRandom.Next(0, (int)bodyNumeric.Maximum);

            bufferHeal[selectID] = healRandom.Next(0, 350);
            bufferFist[selectID] = statRandom.Next(30, 150);
            bufferKick[selectID] = statRandom.Next(30, 150);
            bufferToss[selectID] = statRandom.Next(30, 150);
            bufferWeap[selectID] = statRandom.Next(30, 150);
            bufferWeaT[selectID] = statRandom.Next(30, 150);
            bufferAgil[selectID] = statRandom.Next(30, 150);
            bufferJump[selectID] = statRandom.Next(30, 150);
            bufferReve[selectID] = statRandom.Next(30, 150);
            bufferDefe[selectID] = statRandom.Next(30, 150);

            bufferSp1[selectID] = statRandom.Next(0, 83);
            bufferSp2[selectID] = statRandom.Next(0, 83);
            bufferSp3[selectID] = statRandom.Next(0, 83);

            sectorsInitiliaze();
        }

        private void spc1Button_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(spcAbout1[selectID], "About special move", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void spc2Button_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(spcAbout2[selectID], "About special move", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void spc3Button_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(spcAbout1[selectID], "About special move", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void specialNumeric1_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (charList.Items.Contains(richTextBox1.Text) == true)
            {
                foundBool = true;
                charList.SelectedItem = richTextBox1.Text;
            }
        }

        private void charList_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;

            // задаем параметры для рисования текущего элемента
            // Если элемент имеет состояние 'выбран' мы изменяем цвет его кисти бэкграунда на желтую, 
            //остальные параемтры оставим по умолчанию взятые из параметра `e`
            if (foundBool == true)
            {
                if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                {
                    e = new DrawItemEventArgs(e.Graphics,
                              e.Font,
                              e.Bounds,
                              e.Index,
                              e.State ^ DrawItemState.Selected,
                              e.ForeColor,
                              Color.Green);// <- обратить внимание сюда
                }
            }
            else
            {
                if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                {
                    e = new DrawItemEventArgs(e.Graphics,
                              e.Font,
                              e.Bounds,
                              e.Index,
                              e.State ^ DrawItemState.Selected,
                              e.ForeColor,
                              Color.SkyBlue);// <- обратить внимание сюда
                }
            }

            // Начинаем рисовать
            // Рисуем Background для каждого элемента
            e.DrawBackground();

            // Рисуем текст для каждого элемента
            if (foundBool == true) e.Graphics.DrawString(charList.Items[e.Index].ToString(), e.Font, Brushes.DarkSlateGray, e.Bounds, StringFormat.GenericDefault);
            if (foundBool == false) e.Graphics.DrawString(charList.Items[e.Index].ToString(), e.Font, Brushes.DarkSlateGray, e.Bounds, StringFormat.GenericDefault);
            // если у текущеного элемента есть фокус, эта функция нарисует фокус
            e.DrawFocusRectangle();
        }

        private string charTranslate(string stringTranslate)
        {
            string outString = "";
            byte[] inString = Encoding.UTF8.GetBytes(stringTranslate);

            for (int i = 0; i < inString.Length; i++)
            {
                if (inString[i] >= 33 && inString[i] <= 270)
                {
                    outString += (char)(inString[i] + 65248);
                }
                else if (inString[i] == 32)
                {
                    outString += (char)(inString[12288]);
                }
            }
            return outString;
        }

        private void nameBox_TextChanged(object sender, EventArgs e)
        {
            if (nameBox.Text.Length == 0)
            {
                saveButton.Enabled = false;
                label12.ForeColor = Color.Red;
            }
            else
            {
                saveButton.Enabled = true;
                label12.ForeColor = Color.Black;
                bufferName[selectID] = nameBox.Text;
            }
        }

        private void skinAdd_Click(object sender, EventArgs e)
        {
            ++skinNumeric.Maximum;

            bufferRed.Add(0);
            bufferBlue.Add(0);
            bufferGreen.Add(0);

            bufferRedB.Add(0);
            bufferBlueB.Add(0);
            bufferGreenB.Add(0);

            borderColorButton.BackColor = Color.FromArgb(bufferRedB[(int)skinNumeric.Maximum], bufferGreenB[(int)skinNumeric.Maximum], bufferBlueB[(int)skinNumeric.Maximum]);
            skinColorButton.BackColor = Color.FromArgb(bufferRed[(int)skinNumeric.Maximum], bufferGreen[(int)skinNumeric.Maximum], bufferBlue[(int)skinNumeric.Maximum]);

            skinNumeric.Value = skinNumeric.Maximum;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bufferName.Add("Blank");
            bufferFace.Add(10);
            bufferHead.Add(0);
            bufferBody.Add(1);
            bufferSkin.Add(0);
            bufferHeal.Add(100);
            bufferFist.Add(100);
            bufferKick.Add(100);
            bufferToss.Add(100);
            bufferWeap.Add(100);
            bufferWeaT.Add(100);
            bufferAgil.Add(100);
            bufferJump.Add(100);
            bufferReve.Add(100);
            bufferDefe.Add(100);

            bufferSp1.Add(0);
            bufferSp2.Add(0);
            bufferSp3.Add(0);

            charList.Items.Add("Blank");
            charList.SelectedIndex = charList.Items.Count - 1;

            label20.Text = charList.Items.Count.ToString();
        }

        private void healthNumeric_ValueChanged(object sender, EventArgs e)
        {
            bufferHeal[selectID] = (int)healthNumeric.Value;
        }

        private void kickNumeric_ValueChanged(object sender, EventArgs e)
        {
            bufferKick[selectID] = (int)kickNumeric.Value;
        }

        private void punchNumeric_ValueChanged(object sender, EventArgs e)
        {
            bufferFist[selectID] = (int)punchNumeric.Value;
        }

        private void tossNumeric_ValueChanged(object sender, EventArgs e)
        {
            bufferToss[selectID] = (int)tossNumeric.Value;
        }

        private void weaponNumeric_ValueChanged(object sender, EventArgs e)
        {
            bufferWeap[selectID] = (int)weaponNumeric.Value;
        }

        private void reverseNumeric_ValueChanged(object sender, EventArgs e)
        {
            bufferReve[selectID] = (int)reverseNumeric.Value;
        }

        private void defenseNumeric_ValueChanged(object sender, EventArgs e)
        {
            bufferDefe[selectID] = (int)defenseNumeric.Value;
        }

        private void jumpNumeric_ValueChanged(object sender, EventArgs e)
        {
            bufferJump[selectID] = (int)jumpNumeric.Value;
        }

        private void agilityNumeric_ValueChanged(object sender, EventArgs e)
        {
            bufferAgil[selectID] = (int)agilityNumeric.Value;
        }

        private void weaponTNumeric_ValueChanged(object sender, EventArgs e)
        {
            bufferWeaT[selectID] = (int)weaponTNumeric.Value;
        }

        private void specialNumeric1_SelectedIndexChanged(object sender, EventArgs e)
        {
            bufferSp1[selectID] = specialNumeric1.SelectedIndex;

            if (specialNumeric1.SelectedIndex == 73) { bufferSp1[selectID] = 130; }
            if (specialNumeric1.SelectedIndex == 74) { bufferSp1[selectID] = 131; }
            if (specialNumeric1.SelectedIndex == 75) { bufferSp1[selectID] = 132; }
            if (specialNumeric1.SelectedIndex == 76) { bufferSp1[selectID] = 133; }
            if (specialNumeric1.SelectedIndex == 77) { bufferSp1[selectID] = 134; }
            if (specialNumeric1.SelectedIndex == 78) { bufferSp1[selectID] = 135; }
            if (specialNumeric1.SelectedIndex == 79) { bufferSp1[selectID] = 136; }
            if (specialNumeric1.SelectedIndex == 80) { bufferSp1[selectID] = 137; }
            if (specialNumeric1.SelectedIndex == 81) { bufferSp1[selectID] = 138; }
            if (specialNumeric1.SelectedIndex == 82) { bufferSp1[selectID] = 140; }

            if (bufferSp1[selectID] == 255) { specialNumeric1.SelectedIndex = 83; }
        }

        private void specialNumeric2_SelectedIndexChanged(object sender, EventArgs e)
        {
            bufferSp2[selectID] = specialNumeric2.SelectedIndex;

            if (specialNumeric2.SelectedIndex == 73) { bufferSp2[selectID] = 130; }
            if (specialNumeric2.SelectedIndex == 74) { bufferSp2[selectID] = 131; }
            if (specialNumeric2.SelectedIndex == 75) { bufferSp2[selectID] = 132; }
            if (specialNumeric2.SelectedIndex == 76) { bufferSp2[selectID] = 133; }
            if (specialNumeric2.SelectedIndex == 77) { bufferSp2[selectID] = 134; }
            if (specialNumeric2.SelectedIndex == 78) { bufferSp2[selectID] = 135; }
            if (specialNumeric2.SelectedIndex == 79) { bufferSp2[selectID] = 136; }
            if (specialNumeric2.SelectedIndex == 80) { bufferSp2[selectID] = 137; }
            if (specialNumeric2.SelectedIndex == 81) { bufferSp2[selectID] = 138; }
            if (specialNumeric2.SelectedIndex == 82) { bufferSp2[selectID] = 140; }

            if (bufferSp2[selectID] == 255) { specialNumeric2.SelectedIndex = 83; }
        }

        private void specialNumeric3_SelectedIndexChanged(object sender, EventArgs e)
        {
            bufferSp3[selectID] = specialNumeric3.SelectedIndex;

            if (specialNumeric3.SelectedIndex == 73) { bufferSp3[selectID] = 130; }
            if (specialNumeric3.SelectedIndex == 74) { bufferSp3[selectID] = 131; }
            if (specialNumeric3.SelectedIndex == 75) { bufferSp3[selectID] = 132; }
            if (specialNumeric3.SelectedIndex == 76) { bufferSp3[selectID] = 133; }
            if (specialNumeric3.SelectedIndex == 77) { bufferSp3[selectID] = 134; }
            if (specialNumeric3.SelectedIndex == 78) { bufferSp3[selectID] = 135; }
            if (specialNumeric3.SelectedIndex == 79) { bufferSp3[selectID] = 136; }
            if (specialNumeric3.SelectedIndex == 80) { bufferSp3[selectID] = 137; }
            if (specialNumeric3.SelectedIndex == 81) { bufferSp3[selectID] = 138; }
            if (specialNumeric3.SelectedIndex == 82) { bufferSp3[selectID] = 140; }

            if (bufferSp3[selectID] == 255) { specialNumeric3.SelectedIndex = 83; }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            File.WriteAllText("CharData.txt", String.Empty, Encoding.GetEncoding(932));
            Thread.Sleep(500);
            Encoding SHIFTJIS = Encoding.GetEncoding(932);
            var charDataFile = File.Open("CharData.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);

            try
            {
                using (StreamReader sr = new StreamReader(charDataFile, Encoding.GetEncoding(932)))
                {
                    StreamWriter sw = new StreamWriter(charDataFile, SHIFTJIS);
                    string lines = sr.ReadToEnd();

                    sw.WriteLine("#START");

                    for (int i = 0; i < charList.Items.Count; i++)
                    {
                        string testSave = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, ", bufferName[i], bufferFace[i], bufferHead[i], bufferSkin[i], bufferBody[i], bufferHeal[i], bufferFist[i], bufferKick[i], bufferToss[i], bufferWeap[i], bufferWeaT[i], bufferAgil[i], bufferJump[i], bufferReve[i], bufferDefe[i], bufferSp1[i], bufferSp2[i], bufferSp3[i]);
                        sw.WriteLine(testSave);
                    }

                    if (sr.EndOfStream) sw.WriteLine("#END");

                    sw.Close();
                    sr.Close();
                }
            }
            catch (Exception d)
            {
                MessageBox.Show(d.Message);
            }
            finally
            {
                MessageBox.Show(Owner, "File was successfully rewrite!", "File was save!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
        }

        private void toJapaneseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bufferName[selectID] = charTranslate(nameBox.Text);
            sectorsInitiliaze();
        }
    }
}
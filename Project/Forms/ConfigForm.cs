﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Resources;
using System.Linq;
using NET_Launcher.Classes;

namespace NET_Launcher.Forms
{
    public partial class ConfigForm : Form
    {
        string[] linesLenght;

        static INIManager cfgmanager = new INIManager(Vars.cfgPath);
        static INIManager lnrmanager = new INIManager(Vars.lnrPath);

        string Zoom;
        bool VsyncWait;
        bool FullScreen;

        public static bool bitMode = Convert.ToBoolean(Convert.ToInt32(lnrmanager.GetPrivateString("GameParameters", "16Bit_Mode")));
        public static bool winMode = Convert.ToBoolean(Convert.ToInt32(lnrmanager.GetPrivateString("GameParameters", "WinXP_Mode")));

        public ConfigForm()
        {
            InitializeComponent();

            Random rnd = new Random();
            int value = rnd.Next(1, 6);

            Image gif = Image.FromFile(Vars.gifPath + value + ".gif");

            if (Vars.enableBool == true)
            {
                FullScreen = Convert.ToBoolean(Convert.ToInt32(cfgmanager.GetPrivateString("DirectDrawConfig", "FullScreen")));
                VsyncWait = Convert.ToBoolean(Convert.ToInt32(cfgmanager.GetPrivateString("DirectDrawConfig", "VsyncWait")));
                Zoom = cfgmanager.GetPrivateString("DirectDrawConfig", "Zoom");

                bitMode = Convert.ToBoolean(Convert.ToInt32(lnrmanager.GetPrivateString("GameParameters", "16Bit_Mode")));
                winMode = Convert.ToBoolean(Convert.ToInt32(lnrmanager.GetPrivateString("GameParameters", "WinXP_Mode")));
            }

            zoomBar.ValueChanged += new EventHandler(CheckedChanged);
            vSync.CheckedChanged += new EventHandler(CheckedChanged);
            fullScreen.CheckedChanged += new EventHandler(CheckedChanged);

            bitBox.CheckedChanged += new EventHandler(CheckedChanged);
            winBox.CheckedChanged += new EventHandler(CheckedChanged);

            zoomBar.Enabled = Vars.enableBool;
            vSync.Enabled = Vars.enableBool;
            fullScreen.Enabled = Vars.enableBool;

            gifBox.Image = gif;
            gifBox.Update();
            gifBox.Refresh();

            editKeysButton.Enabled = Vars.enableBool;

            InitializeTrackBar();
            InitializeCheckBoxes();
        }

        public void InitializeTrackBar()
        {
            zoomBar.Value = Convert.ToInt32(Zoom);
            zoomBar.Update();
        }

        public void InitializeCheckBoxes()
        {
            vSync.Checked = VsyncWait;
            fullScreen.Checked = FullScreen;

            bitBox.Checked = bitMode;
            winBox.Checked = winMode;

            winBox.Update();
            bitBox.Update();

            vSync.Update();
            fullScreen.Update();
        }

        private void CheckedChanged(Object sender, EventArgs e)
        {
            saveButton.Enabled = true;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Vars.enableBool != false)
                {
                    cfgmanager.WritePrivateString("DirectDrawConfig", "Zoom", Convert.ToString(zoomBar.Value));
                    cfgmanager.WritePrivateString("DirectDrawConfig", "VsyncWait", Convert.ToString(Convert.ToInt32(vSync.Checked)));
                    cfgmanager.WritePrivateString("DirectDrawConfig", "FullScreen", Convert.ToString(Convert.ToInt32(fullScreen.Checked)));
                }

                lnrmanager.WritePrivateString("GameParameters", "WinXP_Mode", Convert.ToString(Convert.ToInt32(winBox.Checked)));
                lnrmanager.WritePrivateString("GameParameters", "16Bit_Mode", Convert.ToString(Convert.ToInt32(bitBox.Checked)));

                saveButton.Enabled = false;
            }
            catch(Exception exc)
            {
                MessageBox.Show(exc.Message);
                //Здесь сообщение об ошибке
            }
            finally
            {
            }
        }

        private void editKeysButton_Click(object sender, EventArgs e)
        {
            EditKeys keysForm = new EditKeys();
            keysForm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}

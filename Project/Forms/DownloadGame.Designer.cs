﻿namespace NET_Launcher.Forms
{
    partial class DownloadGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DownloadGame));
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.settingsGroup = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.statusBar = new System.Windows.Forms.Label();
            this.sizeEN = new System.Windows.Forms.Label();
            this.sizeJP = new System.Windows.Forms.Label();
            this.dirLabel = new System.Windows.Forms.Label();
            this.dirBox = new System.Windows.Forms.TextBox();
            this.engRadio = new System.Windows.Forms.RadioButton();
            this.japRadio = new System.Windows.Forms.RadioButton();
            this.downloadButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.folderButton = new System.Windows.Forms.Button();
            this.settingsGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 132);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(256, 23);
            this.progressBar.TabIndex = 0;
            this.progressBar.Visible = false;
            // 
            // settingsGroup
            // 
            this.settingsGroup.Controls.Add(this.checkBox1);
            this.settingsGroup.Controls.Add(this.statusBar);
            this.settingsGroup.Controls.Add(this.sizeEN);
            this.settingsGroup.Controls.Add(this.sizeJP);
            this.settingsGroup.Controls.Add(this.dirLabel);
            this.settingsGroup.Controls.Add(this.dirBox);
            this.settingsGroup.Controls.Add(this.engRadio);
            this.settingsGroup.Controls.Add(this.japRadio);
            this.settingsGroup.Location = new System.Drawing.Point(12, 12);
            this.settingsGroup.Name = "settingsGroup";
            this.settingsGroup.Size = new System.Drawing.Size(256, 114);
            this.settingsGroup.TabIndex = 1;
            this.settingsGroup.TabStop = false;
            this.settingsGroup.Text = "Settings";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.Location = new System.Drawing.Point(9, 68);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(191, 17);
            this.checkBox1.TabIndex = 9;
            this.checkBox1.Text = "Delete old launcher after download";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // statusBar
            // 
            this.statusBar.AutoSize = true;
            this.statusBar.Location = new System.Drawing.Point(61, 90);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(56, 13);
            this.statusBar.TabIndex = 8;
            this.statusBar.Text = "Status Bar";
            this.statusBar.Visible = false;
            // 
            // sizeEN
            // 
            this.sizeEN.AutoSize = true;
            this.sizeEN.Location = new System.Drawing.Point(183, 43);
            this.sizeEN.Name = "sizeEN";
            this.sizeEN.Size = new System.Drawing.Size(23, 13);
            this.sizeEN.TabIndex = 7;
            this.sizeEN.Text = "MB";
            // 
            // sizeJP
            // 
            this.sizeJP.AutoSize = true;
            this.sizeJP.Location = new System.Drawing.Point(183, 22);
            this.sizeJP.Name = "sizeJP";
            this.sizeJP.Size = new System.Drawing.Size(23, 13);
            this.sizeJP.TabIndex = 5;
            this.sizeJP.Text = "MB";
            // 
            // dirLabel
            // 
            this.dirLabel.AutoSize = true;
            this.dirLabel.Location = new System.Drawing.Point(6, 90);
            this.dirLabel.Name = "dirLabel";
            this.dirLabel.Size = new System.Drawing.Size(52, 13);
            this.dirLabel.TabIndex = 3;
            this.dirLabel.Text = "Directory:";
            // 
            // dirBox
            // 
            this.dirBox.BackColor = System.Drawing.SystemColors.Control;
            this.dirBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dirBox.Location = new System.Drawing.Point(64, 91);
            this.dirBox.MaxLength = 2000;
            this.dirBox.Name = "dirBox";
            this.dirBox.Size = new System.Drawing.Size(168, 13);
            this.dirBox.TabIndex = 2;
            // 
            // engRadio
            // 
            this.engRadio.AutoSize = true;
            this.engRadio.Location = new System.Drawing.Point(9, 40);
            this.engRadio.Name = "engRadio";
            this.engRadio.Size = new System.Drawing.Size(144, 17);
            this.engRadio.TabIndex = 1;
            this.engRadio.Text = "English version (by piotyr)";
            this.engRadio.UseVisualStyleBackColor = true;
            this.engRadio.CheckedChanged += new System.EventHandler(this.engRadio_CheckedChanged);
            // 
            // japRadio
            // 
            this.japRadio.AutoSize = true;
            this.japRadio.Checked = true;
            this.japRadio.Location = new System.Drawing.Point(9, 19);
            this.japRadio.Name = "japRadio";
            this.japRadio.Size = new System.Drawing.Size(152, 17);
            this.japRadio.TabIndex = 0;
            this.japRadio.TabStop = true;
            this.japRadio.Text = "Japanese version (Original)";
            this.japRadio.UseVisualStyleBackColor = true;
            this.japRadio.CheckedChanged += new System.EventHandler(this.japRadio_CheckedChanged);
            // 
            // downloadButton
            // 
            this.downloadButton.Location = new System.Drawing.Point(204, 132);
            this.downloadButton.Name = "downloadButton";
            this.downloadButton.Size = new System.Drawing.Size(64, 23);
            this.downloadButton.TabIndex = 2;
            this.downloadButton.Text = "Download";
            this.downloadButton.UseVisualStyleBackColor = true;
            this.downloadButton.Click += new System.EventHandler(this.downloadButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(136, 132);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(62, 23);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // folderButton
            // 
            this.folderButton.Location = new System.Drawing.Point(12, 132);
            this.folderButton.Name = "folderButton";
            this.folderButton.Size = new System.Drawing.Size(62, 23);
            this.folderButton.TabIndex = 4;
            this.folderButton.Text = "Folder";
            this.folderButton.UseVisualStyleBackColor = true;
            this.folderButton.Click += new System.EventHandler(this.folderButton_Click);
            // 
            // DownloadGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(280, 167);
            this.Controls.Add(this.folderButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.downloadButton);
            this.Controls.Add(this.settingsGroup);
            this.Controls.Add(this.progressBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DownloadGame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Download Game";
            this.settingsGroup.ResumeLayout(false);
            this.settingsGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.GroupBox settingsGroup;
        private System.Windows.Forms.RadioButton engRadio;
        private System.Windows.Forms.RadioButton japRadio;
        private System.Windows.Forms.Button downloadButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label sizeEN;
        private System.Windows.Forms.Label sizeJP;
        private System.Windows.Forms.Label dirLabel;
        private System.Windows.Forms.TextBox dirBox;
        private System.Windows.Forms.Button folderButton;
        private System.Windows.Forms.Label statusBar;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}
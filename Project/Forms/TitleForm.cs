﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Net;
using NET_Launcher.Classes;
using NET_Launcher.Forms;
using Microsoft.Win32;

namespace NET_Launcher.Forms
{
    public partial class MainForm : Form
    {
        private string[] directoryFiles = Directory.GetFiles(Vars.packPath);

        public MainForm()
        {
            try
            {
                InitializeComponent();
                Visible = true;
                checkFiles();
            }
            catch(Exception e)
            {
                StackTrace stackTrace = new StackTrace(e, true);

                if(stackTrace.FrameCount > 0)
                {
                    StackFrame frame = stackTrace.GetFrame(0);
                    int errorLine = frame.GetFileLineNumber();
                    string functionName = frame.GetMethod().Name;
                    string exceptionMessage = String.Format("Error in {0} method on line {1}", functionName, errorLine);

                    MessageBox.Show(Owner,"Code: " + e.HResult + "\n" + "Column: " + frame.GetFileColumnNumber() + "\n" + exceptionMessage, "Error: " + e.Source, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            finally
            {
                //Здесь сообщение о успешном запуске программы
            }
        }

        public void checkFiles()
        {
            for (int i = 0; i < directoryFiles.Length; i++)
            {
                welcomeText.Text = "Game data checks: " + directoryFiles[i];

                if (File.Exists(Vars.exeFile))
                {
                    playButton.Enabled = true;
                }
                else
                {
                    welcomeText.ForeColor = Color.Red;
                    playButton.Enabled = false;
                    welcomeText.Update();
                    welcomeText.Refresh();
                }

                if (File.Exists(Vars.cfgFile))
                {
                    Vars.enableBool = true;
                }
                else
                {
                    Vars.enableBool = false;
                }

                Vars.playBool = playButton.Enabled;

                welcomeText.Update();
                welcomeText.Refresh();
            }
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            object bitColor = "";
            object winMode = "";

            if (ConfigForm.bitMode == true) { bitColor = " 16BITCOLOR "; }
            if (ConfigForm.winMode == true) { winMode = " WINXPSP2 "; }

            object valueKey = "$~ RUNASADMIN" + bitColor + winMode + "DWM8And16BitMitigation";
            var key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers", true);

            Registry.SetValue(key.ToString(), Vars.exePath, valueKey);

            playButton.Enabled = false;

            welcomeText.Text = "Game is launch please wait...";
            welcomeText.Update();
            
            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo(Vars.packPath + "\\KD.exe") { UseShellExecute = false };
                startInfo.ErrorDialog = true;

                Process.Start(startInfo);
            }
            catch (Exception errExe)
            {
                welcomeText.Text = errExe.Message;
                Update();
            }

            if (IsKDrunning() == true)
            {

                Application.Exit();
            }

            playButton.Enabled = true;
        }

        private bool IsKDrunning()
        {
            return Process.GetProcessesByName(Vars.exeFile).Length > 0;
        }

        private void configButton_Click(object sender, EventArgs e)
        {
            ConfigForm CfgForm = new ConfigForm();
            CfgForm.ShowDialog(this);
        }

        private void contentButton_Click(object sender, EventArgs e)
        {
            ContentForm contentForm = new ContentForm();
            contentForm.ShowDialog(this);
        }
    }
}
﻿namespace NET_Launcher.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.Launcher_Logo = new System.Windows.Forms.GroupBox();
            this.linkLabel = new System.Windows.Forms.LinkLabel();
            this.welcomeText = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.Menu = new System.Windows.Forms.GroupBox();
            this.exitButton = new System.Windows.Forms.Button();
            this.contentButton = new System.Windows.Forms.Button();
            this.configButton = new System.Windows.Forms.Button();
            this.playButton = new System.Windows.Forms.Button();
            this.About = new System.Windows.Forms.GroupBox();
            this.autorText = new System.Windows.Forms.Label();
            this.Launcher_Logo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Menu.SuspendLayout();
            this.About.SuspendLayout();
            this.SuspendLayout();
            // 
            // Launcher_Logo
            // 
            this.Launcher_Logo.Controls.Add(this.linkLabel);
            this.Launcher_Logo.Controls.Add(this.welcomeText);
            this.Launcher_Logo.Controls.Add(this.pictureBox1);
            this.Launcher_Logo.Controls.Add(this.progressBar);
            this.Launcher_Logo.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::NET_Launcher.Properties.Settings.Default, "LogoText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Launcher_Logo.ForeColor = System.Drawing.SystemColors.ControlText;
            resources.ApplyResources(this.Launcher_Logo, "Launcher_Logo");
            this.Launcher_Logo.Name = "Launcher_Logo";
            this.Launcher_Logo.TabStop = false;
            this.Launcher_Logo.Text = global::NET_Launcher.Properties.Settings.Default.LogoText;
            this.Launcher_Logo.UseCompatibleTextRendering = true;
            // 
            // linkLabel
            // 
            resources.ApplyResources(this.linkLabel, "linkLabel");
            this.linkLabel.Name = "linkLabel";
            this.linkLabel.TabStop = true;
            // 
            // welcomeText
            // 
            this.welcomeText.BackColor = System.Drawing.SystemColors.Control;
            this.welcomeText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.welcomeText.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::NET_Launcher.Properties.Settings.Default, "welcomeText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            resources.ApplyResources(this.welcomeText, "welcomeText");
            this.welcomeText.Name = "welcomeText";
            this.welcomeText.ReadOnly = true;
            this.welcomeText.Text = global::NET_Launcher.Properties.Settings.Default.welcomeText;
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // progressBar
            // 
            this.progressBar.DataBindings.Add(new System.Windows.Forms.Binding("Name", global::NET_Launcher.Properties.Settings.Default, "progressBar", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            resources.ApplyResources(this.progressBar, "progressBar");
            this.progressBar.Name = global::NET_Launcher.Properties.Settings.Default.progressBar;
            this.progressBar.Step = 1;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // Menu
            // 
            this.Menu.Controls.Add(this.exitButton);
            this.Menu.Controls.Add(this.contentButton);
            this.Menu.Controls.Add(this.configButton);
            this.Menu.Controls.Add(this.playButton);
            this.Menu.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::NET_Launcher.Properties.Settings.Default, "MenuText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            resources.ApplyResources(this.Menu, "Menu");
            this.Menu.Name = "Menu";
            this.Menu.TabStop = false;
            this.Menu.Text = global::NET_Launcher.Properties.Settings.Default.MenuText;
            this.Menu.UseCompatibleTextRendering = true;
            // 
            // exitButton
            // 
            this.exitButton.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::NET_Launcher.Properties.Settings.Default, "exitButtonText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            resources.ApplyResources(this.exitButton, "exitButton");
            this.exitButton.Name = "exitButton";
            this.exitButton.Text = global::NET_Launcher.Properties.Settings.Default.exitButtonText;
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // contentButton
            // 
            this.contentButton.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::NET_Launcher.Properties.Settings.Default, "aboutButtonText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            resources.ApplyResources(this.contentButton, "contentButton");
            this.contentButton.Name = "contentButton";
            this.contentButton.Text = global::NET_Launcher.Properties.Settings.Default.aboutButtonText;
            this.contentButton.UseVisualStyleBackColor = true;
            this.contentButton.Click += new System.EventHandler(this.contentButton_Click);
            // 
            // configButton
            // 
            this.configButton.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::NET_Launcher.Properties.Settings.Default, "configButtonText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            resources.ApplyResources(this.configButton, "configButton");
            this.configButton.Name = "configButton";
            this.configButton.Text = global::NET_Launcher.Properties.Settings.Default.configButtonText;
            this.configButton.UseVisualStyleBackColor = true;
            this.configButton.Click += new System.EventHandler(this.configButton_Click);
            // 
            // playButton
            // 
            this.playButton.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::NET_Launcher.Properties.Settings.Default, "playButtonText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            resources.ApplyResources(this.playButton, "playButton");
            this.playButton.Name = "playButton";
            this.playButton.Text = global::NET_Launcher.Properties.Settings.Default.playButtonText;
            this.playButton.UseVisualStyleBackColor = true;
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            // 
            // About
            // 
            this.About.Controls.Add(this.autorText);
            this.About.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::NET_Launcher.Properties.Settings.Default, "AboutText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            resources.ApplyResources(this.About, "About");
            this.About.Name = "About";
            this.About.TabStop = false;
            this.About.Text = global::NET_Launcher.Properties.Settings.Default.AboutText;
            this.About.UseCompatibleTextRendering = true;
            // 
            // autorText
            // 
            resources.ApplyResources(this.autorText, "autorText");
            this.autorText.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::NET_Launcher.Properties.Settings.Default, "autorText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.autorText.Name = "autorText";
            this.autorText.Text = global::NET_Launcher.Properties.Settings.Default.autorText;
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.About);
            this.Controls.Add(this.Menu);
            this.Controls.Add(this.Launcher_Logo);
            this.DataBindings.Add(new System.Windows.Forms.Binding("StartPosition", global::NET_Launcher.Properties.Settings.Default, "Position", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::NET_Launcher.Properties.Settings.Default, "TitlebarText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = global::NET_Launcher.Properties.Settings.Default.Position;
            this.Text = global::NET_Launcher.Properties.Settings.Default.TitlebarText;
            this.Launcher_Logo.ResumeLayout(false);
            this.Launcher_Logo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Menu.ResumeLayout(false);
            this.About.ResumeLayout(false);
            this.About.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Launcher_Logo;
        private new System.Windows.Forms.GroupBox Menu;
        private System.Windows.Forms.GroupBox About;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button contentButton;
        private System.Windows.Forms.Button configButton;
        private System.Windows.Forms.Button playButton;
        private System.Windows.Forms.Label autorText;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.LinkLabel linkLabel;
        private System.Windows.Forms.TextBox welcomeText;
    }
}


﻿namespace NET_Launcher.Forms
{
    partial class ConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigForm));
            this.gifGroup = new System.Windows.Forms.GroupBox();
            this.gifBox = new System.Windows.Forms.PictureBox();
            this.configGroup = new System.Windows.Forms.GroupBox();
            this.vSync = new System.Windows.Forms.CheckBox();
            this.fullScreen = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.zoomBar = new System.Windows.Forms.TrackBar();
            this.saveButton = new System.Windows.Forms.Button();
            this.editKeysButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.launcherPage = new System.Windows.Forms.TabPage();
            this.launcherGroup = new System.Windows.Forms.GroupBox();
            this.bitBox = new System.Windows.Forms.CheckBox();
            this.winBox = new System.Windows.Forms.CheckBox();
            this.gifGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gifBox)).BeginInit();
            this.configGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomBar)).BeginInit();
            this.tabControl.SuspendLayout();
            this.launcherPage.SuspendLayout();
            this.launcherGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // gifGroup
            // 
            this.gifGroup.Controls.Add(this.gifBox);
            resources.ApplyResources(this.gifGroup, "gifGroup");
            this.gifGroup.Name = "gifGroup";
            this.gifGroup.TabStop = false;
            // 
            // gifBox
            // 
            resources.ApplyResources(this.gifBox, "gifBox");
            this.gifBox.Name = "gifBox";
            this.gifBox.TabStop = false;
            // 
            // configGroup
            // 
            this.configGroup.Controls.Add(this.vSync);
            this.configGroup.Controls.Add(this.fullScreen);
            this.configGroup.Controls.Add(this.label1);
            this.configGroup.Controls.Add(this.zoomBar);
            resources.ApplyResources(this.configGroup, "configGroup");
            this.configGroup.Name = "configGroup";
            this.configGroup.TabStop = false;
            // 
            // vSync
            // 
            resources.ApplyResources(this.vSync, "vSync");
            this.vSync.Name = "vSync";
            this.vSync.UseCompatibleTextRendering = true;
            this.vSync.UseVisualStyleBackColor = true;
            // 
            // fullScreen
            // 
            resources.ApplyResources(this.fullScreen, "fullScreen");
            this.fullScreen.Name = "fullScreen";
            this.fullScreen.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // zoomBar
            // 
            resources.ApplyResources(this.zoomBar, "zoomBar");
            this.zoomBar.Maximum = 4;
            this.zoomBar.Name = "zoomBar";
            // 
            // saveButton
            // 
            resources.ApplyResources(this.saveButton, "saveButton");
            this.saveButton.Name = "saveButton";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // editKeysButton
            // 
            this.editKeysButton.AutoEllipsis = true;
            resources.ApplyResources(this.editKeysButton, "editKeysButton");
            this.editKeysButton.Name = "editKeysButton";
            this.editKeysButton.UseVisualStyleBackColor = true;
            this.editKeysButton.Click += new System.EventHandler(this.editKeysButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.cancelButton, "cancelButton");
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.launcherPage);
            resources.ApplyResources(this.tabControl, "tabControl");
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            // 
            // launcherPage
            // 
            this.launcherPage.BackColor = System.Drawing.Color.Transparent;
            this.launcherPage.Controls.Add(this.launcherGroup);
            resources.ApplyResources(this.launcherPage, "launcherPage");
            this.launcherPage.Name = "launcherPage";
            // 
            // launcherGroup
            // 
            this.launcherGroup.Controls.Add(this.bitBox);
            this.launcherGroup.Controls.Add(this.winBox);
            resources.ApplyResources(this.launcherGroup, "launcherGroup");
            this.launcherGroup.Name = "launcherGroup";
            this.launcherGroup.TabStop = false;
            // 
            // bitBox
            // 
            resources.ApplyResources(this.bitBox, "bitBox");
            this.bitBox.Checked = true;
            this.bitBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.bitBox.Name = "bitBox";
            this.bitBox.UseVisualStyleBackColor = true;
            // 
            // winBox
            // 
            resources.ApplyResources(this.winBox, "winBox");
            this.winBox.Checked = true;
            this.winBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.winBox.Name = "winBox";
            this.winBox.UseVisualStyleBackColor = true;
            // 
            // ConfigForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.editKeysButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.configGroup);
            this.Controls.Add(this.gifGroup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigForm";
            this.TopMost = true;
            this.gifGroup.ResumeLayout(false);
            this.gifGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gifBox)).EndInit();
            this.configGroup.ResumeLayout(false);
            this.configGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomBar)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.launcherPage.ResumeLayout(false);
            this.launcherGroup.ResumeLayout(false);
            this.launcherGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gifGroup;
        private System.Windows.Forms.PictureBox gifBox;
        private System.Windows.Forms.GroupBox configGroup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button editKeysButton;
        private System.Windows.Forms.Button cancelButton;
        public System.Windows.Forms.TabControl tabControl;
        public System.Windows.Forms.TabPage launcherPage;
        public System.Windows.Forms.GroupBox launcherGroup;
        public System.Windows.Forms.CheckBox bitBox;
        public System.Windows.Forms.CheckBox winBox;
        public System.Windows.Forms.CheckBox vSync;
        public System.Windows.Forms.CheckBox fullScreen;
        public System.Windows.Forms.TrackBar zoomBar;
    }
}
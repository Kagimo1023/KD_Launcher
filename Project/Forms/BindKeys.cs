﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using NET_Launcher.Classes;

namespace NET_Launcher.Forms
{
    public partial class bindForm : Form
    {
        INIManager manager = new INIManager(Vars.cfgPath);
        messageCall messageCall = new messageCall();

        public bindForm(string ButtonData)
        {
            InitializeComponent();

            this.BringToFront();
            this.Focus();

            this.Text = ButtonData;

            string myString = manager.GetPrivateString(Vars.resultString, Vars.Key);

            KeyUp += new KeyEventHandler(bindform_KeyUp);
            KeyDown += new KeyEventHandler(bindForm_KeyDown);

            saveButton.Click += EditKeys.bindForm_Closed;
            saveButton.Click += new EventHandler(saveButton_Click);

            saveButton.KeyDown += new KeyEventHandler(bindForm_KeyDown);
            cancelButton.KeyDown += new KeyEventHandler(bindForm_KeyDown);

            saveButton.KeyUp += new KeyEventHandler(bindform_KeyUp);
            cancelButton.KeyUp += new KeyEventHandler(bindform_KeyUp);

            saveButton.PreviewKeyDown += new PreviewKeyDownEventHandler(bindForm_PreviewKeyDown);
            cancelButton.PreviewKeyDown += new PreviewKeyDownEventHandler(bindForm_PreviewKeyDown);

            PreviewKeyDown += new PreviewKeyDownEventHandler(bindForm_PreviewKeyDown);

            oCharText.Text = Vars.Key;
            oCodeText.Text = myString;

            nCharText.Text = Vars.Key;
            nCodeText.Text = myString;

            checkKeys();
        }

        private void checkKeys()
        {
            nCharText.ForeColor = Color.Green;
            nCodeText.ForeColor = Color.Green;

            pressText.Text = "Please press any key...";

            for (int i = 0; i <= 10; ++i)
            {
                if (nCodeText.Text == EditKeys.keysDictionary[i])
                {
                    nCodeText.ForeColor = Color.Red;
                    nCharText.ForeColor = Color.Red;
                    pressText.Text = "Thats button is busy...";
                    saveButton.Enabled = false;
                }
            }
        }

        private void bindform_KeyUp(object sender, KeyEventArgs e)
        {
            cancelButton.Enabled = true;
            saveButton.Enabled = true;
        }

        private void bindForm_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            saveButton.Enabled = false;
            cancelButton.Enabled = false;

            e.IsInputKey = false;

            checkKeys();

            nCodeText.Text = e.KeyValue.ToString();
            nCharText.Text = e.KeyCode.ToString();

            Vars.KeyData = e.KeyValue.ToString();
        }

        private void bindForm_KeyDown(object sender, KeyEventArgs e)
        {
            this.Focus();

            saveButton.Enabled = false;
            cancelButton.Enabled = false;

            checkKeys();

            nCodeText.Text = e.KeyValue.ToString();
            nCharText.Text = e.KeyCode.ToString();

            Vars.KeyData = e.KeyValue.ToString();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            Vars.Kdata = Vars.KeyData;
            this.Close();
        }

        private void bindForm_Load(object sender, EventArgs e)
        {

        }
    }
}

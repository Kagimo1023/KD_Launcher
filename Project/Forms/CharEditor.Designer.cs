﻿namespace NET_Launcher.Forms
{
    partial class CharEditor
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CharEditor));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.totalLabel = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.charList = new System.Windows.Forms.ListBox();
            this.paramChar = new System.Windows.Forms.GroupBox();
            this.skinAdd = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.specialNumeric3 = new System.Windows.Forms.ComboBox();
            this.specialNumeric2 = new System.Windows.Forms.ComboBox();
            this.specialNumeric1 = new System.Windows.Forms.ComboBox();
            this.spc3Button = new System.Windows.Forms.Button();
            this.spc2Button = new System.Windows.Forms.Button();
            this.spc1Button = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.facePicture = new System.Windows.Forms.PictureBox();
            this.headPicture = new System.Windows.Forms.PictureBox();
            this.bodyPicture = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.skinColorButton = new System.Windows.Forms.Button();
            this.borderColorButton = new System.Windows.Forms.Button();
            this.skinNumeric = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.nameBox = new System.Windows.Forms.RichTextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toJapaneseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bodyNumeric = new System.Windows.Forms.NumericUpDown();
            this.headNumeric = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.faceNumeric = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.weaponTNumeric = new System.Windows.Forms.NumericUpDown();
            this.agilityNumeric = new System.Windows.Forms.NumericUpDown();
            this.jumpNumeric = new System.Windows.Forms.NumericUpDown();
            this.defenseNumeric = new System.Windows.Forms.NumericUpDown();
            this.reverseNumeric = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.healthNumeric = new System.Windows.Forms.NumericUpDown();
            this.punchNumeric = new System.Windows.Forms.NumericUpDown();
            this.kickNumeric = new System.Windows.Forms.NumericUpDown();
            this.tossNumeric = new System.Windows.Forms.NumericUpDown();
            this.weaponNumeric = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.colorDialog2 = new System.Windows.Forms.ColorDialog();
            this.button3 = new System.Windows.Forms.Button();
            this.toRussiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.paramChar.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.facePicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bodyPicture)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinNumeric)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bodyNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.faceNumeric)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weaponTNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.agilityNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jumpNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defenseNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reverseNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.healthNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.punchNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kickNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tossNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weaponNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.totalLabel);
            this.groupBox1.Controls.Add(this.richTextBox1);
            this.groupBox1.Controls.Add(this.charList);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(116, 425);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Char list:";
            this.groupBox1.UseCompatibleTextRendering = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(45, 403);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(13, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "0";
            // 
            // totalLabel
            // 
            this.totalLabel.AutoSize = true;
            this.totalLabel.Location = new System.Drawing.Point(6, 403);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Size = new System.Drawing.Size(37, 13);
            this.totalLabel.TabIndex = 4;
            this.totalLabel.Text = "Total: ";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(6, 17);
            this.richTextBox1.Multiline = false;
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox1.Size = new System.Drawing.Size(104, 19);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            this.richTextBox1.WordWrap = false;
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // charList
            // 
            this.charList.FormattingEnabled = true;
            this.charList.Location = new System.Drawing.Point(6, 39);
            this.charList.Name = "charList";
            this.charList.ScrollAlwaysVisible = true;
            this.charList.Size = new System.Drawing.Size(104, 355);
            this.charList.TabIndex = 0;
            this.charList.SelectedIndexChanged += new System.EventHandler(this.charList_SelectedIndexChanged);
            // 
            // paramChar
            // 
            this.paramChar.Controls.Add(this.skinAdd);
            this.paramChar.Controls.Add(this.label13);
            this.paramChar.Controls.Add(this.panel4);
            this.paramChar.Controls.Add(this.panel3);
            this.paramChar.Controls.Add(this.label16);
            this.paramChar.Controls.Add(this.panel2);
            this.paramChar.Controls.Add(this.panel1);
            this.paramChar.Location = new System.Drawing.Point(146, 12);
            this.paramChar.Name = "paramChar";
            this.paramChar.Size = new System.Drawing.Size(322, 425);
            this.paramChar.TabIndex = 1;
            this.paramChar.TabStop = false;
            this.paramChar.Text = "Parameters:";
            this.paramChar.UseCompatibleTextRendering = true;
            // 
            // skinAdd
            // 
            this.skinAdd.Location = new System.Drawing.Point(210, 154);
            this.skinAdd.Name = "skinAdd";
            this.skinAdd.Size = new System.Drawing.Size(99, 22);
            this.skinAdd.TabIndex = 20;
            this.skinAdd.Text = "New Skin";
            this.skinAdd.UseVisualStyleBackColor = true;
            this.skinAdd.Click += new System.EventHandler(this.skinAdd_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(236, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "Preview";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.specialNumeric3);
            this.panel4.Controls.Add(this.specialNumeric2);
            this.panel4.Controls.Add(this.specialNumeric1);
            this.panel4.Controls.Add(this.spc3Button);
            this.panel4.Controls.Add(this.spc2Button);
            this.panel4.Controls.Add(this.spc1Button);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Location = new System.Drawing.Point(14, 335);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(295, 81);
            this.panel4.TabIndex = 16;
            // 
            // specialNumeric3
            // 
            this.specialNumeric3.DropDownHeight = 70;
            this.specialNumeric3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.specialNumeric3.FormattingEnabled = true;
            this.specialNumeric3.IntegralHeight = false;
            this.specialNumeric3.Location = new System.Drawing.Point(149, 50);
            this.specialNumeric3.MaxDropDownItems = 100;
            this.specialNumeric3.Name = "specialNumeric3";
            this.specialNumeric3.Size = new System.Drawing.Size(114, 21);
            this.specialNumeric3.TabIndex = 49;
            this.specialNumeric3.SelectedIndexChanged += new System.EventHandler(this.specialNumeric3_SelectedIndexChanged);
            // 
            // specialNumeric2
            // 
            this.specialNumeric2.DropDownHeight = 70;
            this.specialNumeric2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.specialNumeric2.FormattingEnabled = true;
            this.specialNumeric2.IntegralHeight = false;
            this.specialNumeric2.Location = new System.Drawing.Point(149, 27);
            this.specialNumeric2.MaxDropDownItems = 100;
            this.specialNumeric2.Name = "specialNumeric2";
            this.specialNumeric2.Size = new System.Drawing.Size(113, 21);
            this.specialNumeric2.TabIndex = 48;
            this.specialNumeric2.SelectedIndexChanged += new System.EventHandler(this.specialNumeric2_SelectedIndexChanged);
            // 
            // specialNumeric1
            // 
            this.specialNumeric1.DropDownHeight = 70;
            this.specialNumeric1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.specialNumeric1.FormattingEnabled = true;
            this.specialNumeric1.IntegralHeight = false;
            this.specialNumeric1.Location = new System.Drawing.Point(149, 4);
            this.specialNumeric1.MaxDropDownItems = 100;
            this.specialNumeric1.Name = "specialNumeric1";
            this.specialNumeric1.Size = new System.Drawing.Size(113, 21);
            this.specialNumeric1.TabIndex = 47;
            this.specialNumeric1.SelectedIndexChanged += new System.EventHandler(this.specialNumeric1_SelectedIndexChanged);
            // 
            // spc3Button
            // 
            this.spc3Button.Location = new System.Drawing.Point(268, 49);
            this.spc3Button.Name = "spc3Button";
            this.spc3Button.Size = new System.Drawing.Size(19, 23);
            this.spc3Button.TabIndex = 46;
            this.spc3Button.Text = "?";
            this.spc3Button.UseVisualStyleBackColor = true;
            this.spc3Button.Click += new System.EventHandler(this.spc3Button_Click);
            // 
            // spc2Button
            // 
            this.spc2Button.Location = new System.Drawing.Point(268, 26);
            this.spc2Button.Name = "spc2Button";
            this.spc2Button.Size = new System.Drawing.Size(19, 23);
            this.spc2Button.TabIndex = 45;
            this.spc2Button.Text = "?";
            this.spc2Button.UseVisualStyleBackColor = true;
            this.spc2Button.Click += new System.EventHandler(this.spc2Button_Click);
            // 
            // spc1Button
            // 
            this.spc1Button.Location = new System.Drawing.Point(268, 3);
            this.spc1Button.Name = "spc1Button";
            this.spc1Button.Size = new System.Drawing.Size(19, 23);
            this.spc1Button.TabIndex = 44;
            this.spc1Button.Text = "?";
            this.spc1Button.UseVisualStyleBackColor = true;
            this.spc1Button.Click += new System.EventHandler(this.spc1Button_Click);
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(8, 51);
            this.label3.Margin = new System.Windows.Forms.Padding(5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 20);
            this.label3.TabIndex = 40;
            this.label3.Text = "3) Special Move:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Location = new System.Drawing.Point(8, 28);
            this.label18.Margin = new System.Windows.Forms.Padding(5);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(133, 20);
            this.label18.TabIndex = 39;
            this.label18.Text = "2) Special Move:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label19.Location = new System.Drawing.Point(8, 5);
            this.label19.Margin = new System.Windows.Forms.Padding(5);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(133, 20);
            this.label19.TabIndex = 38;
            this.label19.Text = "1) Special Move:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Green;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.facePicture);
            this.panel3.Controls.Add(this.headPicture);
            this.panel3.Controls.Add(this.bodyPicture);
            this.panel3.Location = new System.Drawing.Point(210, 39);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(100, 112);
            this.panel3.TabIndex = 15;
            // 
            // facePicture
            // 
            this.facePicture.BackColor = System.Drawing.Color.Green;
            this.facePicture.InitialImage = null;
            this.facePicture.Location = new System.Drawing.Point(25, 5);
            this.facePicture.Name = "facePicture";
            this.facePicture.Size = new System.Drawing.Size(42, 24);
            this.facePicture.TabIndex = 1;
            this.facePicture.TabStop = false;
            // 
            // headPicture
            // 
            this.headPicture.BackColor = System.Drawing.Color.Green;
            this.headPicture.InitialImage = null;
            this.headPicture.Location = new System.Drawing.Point(25, 5);
            this.headPicture.Name = "headPicture";
            this.headPicture.Size = new System.Drawing.Size(48, 24);
            this.headPicture.TabIndex = 2;
            this.headPicture.TabStop = false;
            this.headPicture.Visible = false;
            // 
            // bodyPicture
            // 
            this.bodyPicture.Location = new System.Drawing.Point(12, 29);
            this.bodyPicture.Name = "bodyPicture";
            this.bodyPicture.Size = new System.Drawing.Size(72, 72);
            this.bodyPicture.TabIndex = 0;
            this.bodyPicture.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(99, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(31, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "Base";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.skinColorButton);
            this.panel2.Controls.Add(this.borderColorButton);
            this.panel2.Controls.Add(this.skinNumeric);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.nameBox);
            this.panel2.Controls.Add(this.bodyNumeric);
            this.panel2.Controls.Add(this.headNumeric);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.faceNumeric);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(14, 39);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(178, 158);
            this.panel2.TabIndex = 11;
            // 
            // skinColorButton
            // 
            this.skinColorButton.Location = new System.Drawing.Point(146, 109);
            this.skinColorButton.Name = "skinColorButton";
            this.skinColorButton.Size = new System.Drawing.Size(19, 22);
            this.skinColorButton.TabIndex = 27;
            this.skinColorButton.UseVisualStyleBackColor = true;
            this.skinColorButton.Click += new System.EventHandler(this.skinColorButton_Click);
            // 
            // borderColorButton
            // 
            this.borderColorButton.Location = new System.Drawing.Point(121, 109);
            this.borderColorButton.Name = "borderColorButton";
            this.borderColorButton.Size = new System.Drawing.Size(19, 22);
            this.borderColorButton.TabIndex = 26;
            this.borderColorButton.UseVisualStyleBackColor = true;
            this.borderColorButton.Click += new System.EventHandler(this.borderColorButton_Click);
            // 
            // skinNumeric
            // 
            this.skinNumeric.Location = new System.Drawing.Point(64, 110);
            this.skinNumeric.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.skinNumeric.Name = "skinNumeric";
            this.skinNumeric.Size = new System.Drawing.Size(50, 20);
            this.skinNumeric.TabIndex = 25;
            this.skinNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.skinNumeric.ValueChanged += new System.EventHandler(this.skinNumeric_ValueChanged);
            // 
            // label21
            // 
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label21.Location = new System.Drawing.Point(9, 110);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 20);
            this.label21.TabIndex = 24;
            this.label21.Text = "Skin:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label21.UseCompatibleTextRendering = true;
            // 
            // nameBox
            // 
            this.nameBox.ContextMenuStrip = this.contextMenuStrip1;
            this.nameBox.Location = new System.Drawing.Point(64, 6);
            this.nameBox.MaxLength = 8;
            this.nameBox.Multiline = false;
            this.nameBox.Name = "nameBox";
            this.nameBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.nameBox.Size = new System.Drawing.Size(101, 20);
            this.nameBox.TabIndex = 23;
            this.nameBox.Text = "";
            this.nameBox.TextChanged += new System.EventHandler(this.nameBox_TextChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toJapaneseToolStripMenuItem,
            this.toRussiaToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(181, 70);
            // 
            // toJapaneseToolStripMenuItem
            // 
            this.toJapaneseToolStripMenuItem.Name = "toJapaneseToolStripMenuItem";
            this.toJapaneseToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.toJapaneseToolStripMenuItem.Text = "To Japanese";
            this.toJapaneseToolStripMenuItem.Click += new System.EventHandler(this.toJapaneseToolStripMenuItem_Click);
            // 
            // bodyNumeric
            // 
            this.bodyNumeric.Location = new System.Drawing.Point(64, 84);
            this.bodyNumeric.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.bodyNumeric.Name = "bodyNumeric";
            this.bodyNumeric.Size = new System.Drawing.Size(100, 20);
            this.bodyNumeric.TabIndex = 22;
            this.bodyNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bodyNumeric.ValueChanged += new System.EventHandler(this.bodyNumeric_ValueChangedUpDown);
            // 
            // headNumeric
            // 
            this.headNumeric.Location = new System.Drawing.Point(64, 58);
            this.headNumeric.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.headNumeric.Name = "headNumeric";
            this.headNumeric.Size = new System.Drawing.Size(100, 20);
            this.headNumeric.TabIndex = 18;
            this.headNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Location = new System.Drawing.Point(9, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 20);
            this.label12.TabIndex = 13;
            this.label12.Text = "Name:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label12.UseCompatibleTextRendering = true;
            // 
            // faceNumeric
            // 
            this.faceNumeric.Location = new System.Drawing.Point(64, 32);
            this.faceNumeric.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.faceNumeric.Name = "faceNumeric";
            this.faceNumeric.Size = new System.Drawing.Size(100, 20);
            this.faceNumeric.TabIndex = 17;
            this.faceNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.faceNumeric.ValueChanged += new System.EventHandler(this.faceNumeric_ValueChangedUpDown);
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Location = new System.Drawing.Point(9, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 20);
            this.label4.TabIndex = 16;
            this.label4.Text = "Body:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.UseCompatibleTextRendering = true;
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(9, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 20);
            this.label2.TabIndex = 14;
            this.label2.Text = "Head:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.UseCompatibleTextRendering = true;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(9, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 20);
            this.label1.TabIndex = 13;
            this.label1.Text = "Face:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.UseCompatibleTextRendering = true;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.weaponTNumeric);
            this.panel1.Controls.Add(this.agilityNumeric);
            this.panel1.Controls.Add(this.jumpNumeric);
            this.panel1.Controls.Add(this.defenseNumeric);
            this.panel1.Controls.Add(this.reverseNumeric);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.healthNumeric);
            this.panel1.Controls.Add(this.punchNumeric);
            this.panel1.Controls.Add(this.kickNumeric);
            this.panel1.Controls.Add(this.tossNumeric);
            this.panel1.Controls.Add(this.weaponNumeric);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Location = new System.Drawing.Point(14, 203);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(296, 126);
            this.panel1.TabIndex = 10;
            // 
            // weaponTNumeric
            // 
            this.weaponTNumeric.Location = new System.Drawing.Point(223, 5);
            this.weaponTNumeric.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.weaponTNumeric.Name = "weaponTNumeric";
            this.weaponTNumeric.Size = new System.Drawing.Size(55, 20);
            this.weaponTNumeric.TabIndex = 37;
            this.weaponTNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.weaponTNumeric.ValueChanged += new System.EventHandler(this.weaponTNumeric_ValueChanged);
            // 
            // agilityNumeric
            // 
            this.agilityNumeric.Location = new System.Drawing.Point(223, 28);
            this.agilityNumeric.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.agilityNumeric.Name = "agilityNumeric";
            this.agilityNumeric.Size = new System.Drawing.Size(55, 20);
            this.agilityNumeric.TabIndex = 36;
            this.agilityNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.agilityNumeric.ValueChanged += new System.EventHandler(this.agilityNumeric_ValueChanged);
            // 
            // jumpNumeric
            // 
            this.jumpNumeric.Location = new System.Drawing.Point(223, 51);
            this.jumpNumeric.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.jumpNumeric.Name = "jumpNumeric";
            this.jumpNumeric.Size = new System.Drawing.Size(55, 20);
            this.jumpNumeric.TabIndex = 35;
            this.jumpNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.jumpNumeric.ValueChanged += new System.EventHandler(this.jumpNumeric_ValueChanged);
            // 
            // defenseNumeric
            // 
            this.defenseNumeric.Location = new System.Drawing.Point(223, 74);
            this.defenseNumeric.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.defenseNumeric.Name = "defenseNumeric";
            this.defenseNumeric.Size = new System.Drawing.Size(55, 20);
            this.defenseNumeric.TabIndex = 34;
            this.defenseNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.defenseNumeric.ValueChanged += new System.EventHandler(this.defenseNumeric_ValueChanged);
            // 
            // reverseNumeric
            // 
            this.reverseNumeric.Location = new System.Drawing.Point(223, 97);
            this.reverseNumeric.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.reverseNumeric.Name = "reverseNumeric";
            this.reverseNumeric.Size = new System.Drawing.Size(55, 20);
            this.reverseNumeric.TabIndex = 33;
            this.reverseNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.reverseNumeric.ValueChanged += new System.EventHandler(this.reverseNumeric_ValueChanged);
            // 
            // label17
            // 
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Location = new System.Drawing.Point(147, 97);
            this.label17.Margin = new System.Windows.Forms.Padding(5);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 20);
            this.label17.TabIndex = 32;
            this.label17.Text = "Reverse:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // healthNumeric
            // 
            this.healthNumeric.Location = new System.Drawing.Point(86, 5);
            this.healthNumeric.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.healthNumeric.Name = "healthNumeric";
            this.healthNumeric.Size = new System.Drawing.Size(55, 20);
            this.healthNumeric.TabIndex = 31;
            this.healthNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.healthNumeric.ValueChanged += new System.EventHandler(this.healthNumeric_ValueChanged);
            // 
            // punchNumeric
            // 
            this.punchNumeric.Location = new System.Drawing.Point(86, 28);
            this.punchNumeric.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.punchNumeric.Name = "punchNumeric";
            this.punchNumeric.Size = new System.Drawing.Size(55, 20);
            this.punchNumeric.TabIndex = 30;
            this.punchNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.punchNumeric.ValueChanged += new System.EventHandler(this.punchNumeric_ValueChanged);
            // 
            // kickNumeric
            // 
            this.kickNumeric.Location = new System.Drawing.Point(86, 51);
            this.kickNumeric.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.kickNumeric.Name = "kickNumeric";
            this.kickNumeric.Size = new System.Drawing.Size(55, 20);
            this.kickNumeric.TabIndex = 29;
            this.kickNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.kickNumeric.ValueChanged += new System.EventHandler(this.kickNumeric_ValueChanged);
            // 
            // tossNumeric
            // 
            this.tossNumeric.Location = new System.Drawing.Point(86, 74);
            this.tossNumeric.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.tossNumeric.Name = "tossNumeric";
            this.tossNumeric.Size = new System.Drawing.Size(55, 20);
            this.tossNumeric.TabIndex = 28;
            this.tossNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tossNumeric.ValueChanged += new System.EventHandler(this.tossNumeric_ValueChanged);
            // 
            // weaponNumeric
            // 
            this.weaponNumeric.Location = new System.Drawing.Point(86, 97);
            this.weaponNumeric.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.weaponNumeric.Name = "weaponNumeric";
            this.weaponNumeric.Size = new System.Drawing.Size(55, 20);
            this.weaponNumeric.TabIndex = 19;
            this.weaponNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.weaponNumeric.ValueChanged += new System.EventHandler(this.weaponNumeric_ValueChanged);
            // 
            // label14
            // 
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.Location = new System.Drawing.Point(147, 74);
            this.label14.Margin = new System.Windows.Forms.Padding(5);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 20);
            this.label14.TabIndex = 27;
            this.label14.Text = "Defense:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Location = new System.Drawing.Point(147, 51);
            this.label15.Margin = new System.Windows.Forms.Padding(5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 20);
            this.label15.TabIndex = 26;
            this.label15.Text = "Jump:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Location = new System.Drawing.Point(147, 28);
            this.label11.Margin = new System.Windows.Forms.Padding(5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 20);
            this.label11.TabIndex = 25;
            this.label11.Text = "Agility:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Location = new System.Drawing.Point(147, 5);
            this.label10.Margin = new System.Windows.Forms.Padding(5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 20);
            this.label10.TabIndex = 24;
            this.label10.Text = "Weapon T:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Location = new System.Drawing.Point(9, 97);
            this.label9.Margin = new System.Windows.Forms.Padding(5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 20);
            this.label9.TabIndex = 19;
            this.label9.Text = "Weapon:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Location = new System.Drawing.Point(9, 74);
            this.label8.Margin = new System.Windows.Forms.Padding(5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 20);
            this.label8.TabIndex = 18;
            this.label8.Text = "Toss:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(9, 51);
            this.label7.Margin = new System.Windows.Forms.Padding(5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 20);
            this.label7.TabIndex = 17;
            this.label7.Text = "Kick:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(9, 28);
            this.label6.Margin = new System.Windows.Forms.Padding(5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 20);
            this.label6.TabIndex = 16;
            this.label6.Text = "Punch:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(9, 5);
            this.label5.Margin = new System.Windows.Forms.Padding(5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 20);
            this.label5.TabIndex = 15;
            this.label5.Text = "Health:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(401, 443);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(67, 22);
            this.saveButton.TabIndex = 2;
            this.saveButton.Text = "Save";
            this.saveButton.UseCompatibleTextRendering = true;
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(328, 443);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(67, 22);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseCompatibleTextRendering = true;
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(147, 443);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(67, 22);
            this.button1.TabIndex = 4;
            this.button1.Text = "Generate";
            this.button1.UseCompatibleTextRendering = true;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(13, 443);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(109, 22);
            this.button2.TabIndex = 5;
            this.button2.Text = "Create Blank";
            this.button2.UseCompatibleTextRendering = true;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(255, 443);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(67, 22);
            this.button3.TabIndex = 21;
            this.button3.Text = "Default";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // toRussiaToolStripMenuItem
            // 
            this.toRussiaToolStripMenuItem.Name = "toRussiaToolStripMenuItem";
            this.toRussiaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.toRussiaToolStripMenuItem.Text = "To Russian";
            // 
            // CharEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 477);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.paramChar);
            this.Controls.Add(this.groupBox1);
            this.Enabled = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CharEditor";
            this.Text = "CharEditor";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.paramChar.ResumeLayout(false);
            this.paramChar.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.facePicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bodyPicture)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.skinNumeric)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bodyNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.faceNumeric)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.weaponTNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.agilityNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jumpNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defenseNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reverseNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.healthNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.punchNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kickNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tossNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weaponNumeric)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox charList;
        private System.Windows.Forms.GroupBox paramChar;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox facePicture;
        private System.Windows.Forms.PictureBox bodyPicture;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.NumericUpDown bodyNumeric;
        private System.Windows.Forms.NumericUpDown headNumeric;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown faceNumeric;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown weaponTNumeric;
        private System.Windows.Forms.NumericUpDown agilityNumeric;
        private System.Windows.Forms.NumericUpDown jumpNumeric;
        private System.Windows.Forms.NumericUpDown defenseNumeric;
        private System.Windows.Forms.NumericUpDown reverseNumeric;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown healthNumeric;
        private System.Windows.Forms.NumericUpDown punchNumeric;
        private System.Windows.Forms.NumericUpDown kickNumeric;
        private System.Windows.Forms.NumericUpDown tossNumeric;
        private System.Windows.Forms.NumericUpDown weaponNumeric;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.RichTextBox nameBox;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox headPicture;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button borderColorButton;
        private System.Windows.Forms.NumericUpDown skinNumeric;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button skinColorButton;
        private System.Windows.Forms.ColorDialog colorDialog2;
        private System.Windows.Forms.Button skinAdd;
        private System.Windows.Forms.Button spc3Button;
        private System.Windows.Forms.Button spc2Button;
        private System.Windows.Forms.Button spc1Button;
        private System.Windows.Forms.ComboBox specialNumeric3;
        private System.Windows.Forms.ComboBox specialNumeric2;
        private System.Windows.Forms.ComboBox specialNumeric1;
        private System.Windows.Forms.Label totalLabel;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toJapaneseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toRussiaToolStripMenuItem;
    }
}
﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Net;
using System.Threading;
using NET_Launcher.Forms;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;

namespace NET_Launcher.Classes
{
    class Methods
    {
        public static TreeNode madeNode = new TreeNode();

        public static string GetWebFileName(Uri uri)
        {
            string filename = Path.GetFileName(uri.LocalPath);
            return filename;
        }

        public static int SelectPackGet()
        {
            INIManager manager = new INIManager(Vars.lnrPath);
            return Convert.ToInt32(manager.GetPrivateString("Launcher", "DefaultPack"));
        }

        public static string GetFileSize(Uri uriPath)
        {
            var webRequest = HttpWebRequest.Create(uriPath);
            webRequest.Method = "HEAD";

            using (var webResponse = webRequest.GetResponse())
            {
                var fileSize = webResponse.Headers.Get("Content-Length");
                var fileSizeInMegaByte = Math.Round(Convert.ToDouble(fileSize) / 1024.0 / 1024.0, 2);
                return fileSizeInMegaByte + " MB";
            }
        }

        public static string Mediafire(string download)
        {
            HttpWebRequest req;
            HttpWebResponse res;
            string str = "";
            req = (HttpWebRequest)WebRequest.Create(download);
            res = (HttpWebResponse)req.GetResponse();
            str = new StreamReader(res.GetResponseStream()).ReadToEnd();
            int indexurl = str.IndexOf("http://download");
            int indexend = GetNextIndexOf('"', str, indexurl);
            string direct = str.Substring(indexurl, indexend - indexurl);
            return direct;
        }

        static int GetNextIndexOf(char c, string source, int start)
        {
            for (int i = start; i < source.Length; i++)
            {
                if (source[i] == c)
                {
                    return i;
                }
            }
            return -1;
        }

        public static void downloadFile(Uri uri, string fileName, AsyncCompletedEventHandler asyncCompletedEventHandler, DownloadProgressChangedEventHandler downloadProgressChangedEventHandler)
        {
            WebClient client = new WebClient();
            WebRequest request = WebRequest.Create(uri);

            using (WebResponse response = request.GetResponse())
            {
                client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(downloadProgressChangedEventHandler);
                client.DownloadFileCompleted += new AsyncCompletedEventHandler(asyncCompletedEventHandler);
                client.DownloadFileAsync(uri, fileName);
            }
        }

        public static void ExtractZipFile(string archiveFilenameIn, string password, string outFolder)
        {
            ZipFile zf = null;
            try
            {
                FileStream fs = File.OpenRead(archiveFilenameIn);
                zf = new ZipFile(fs);
                if (!String.IsNullOrEmpty(password))
                {
                    zf.Password = password;     // AES encrypted entries are handled automatically
                }
                foreach (ZipEntry zipEntry in zf)
                {
                    if (!zipEntry.IsFile)
                    {
                        continue;           // Ignore directories
                    }
                    String entryFileName = zipEntry.Name;
                    // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                    // Optionally match entrynames against a selection list here to skip as desired.
                    // The unpacked length is available in the zipEntry.Size property.

                    byte[] buffer = new byte[4096];     // 4K is optimum
                    Stream zipStream = zf.GetInputStream(zipEntry);

                    // Manipulate the output filename here as desired.
                    String fullZipToPath = Path.Combine(outFolder, entryFileName);
                    string directoryName = Path.GetDirectoryName(fullZipToPath);
                    if (directoryName.Length > 0)
                        Directory.CreateDirectory(directoryName);

                    // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                    // of the file, but does not waste memory.
                    // The "using" will close the stream even if an exception occurs.
                    using (FileStream streamWriter = File.Create(fullZipToPath))
                    {
                        StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                }
            }
            finally
            {
                if (zf != null)
                {
                    zf.IsStreamOwner = true; // Makes close also shut the underlying stream
                    zf.Close(); // Ensure we release resources
                    File.Delete(archiveFilenameIn);
                }
            }
        }

        public static void DeleteLastLine(string filepath)
        {
            List<string> lines = File.ReadAllLines(filepath).ToList();

            File.WriteAllLines(filepath, lines.GetRange(0, lines.Count - 1).ToArray());

        }

        public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the source directory does not exist, throw an exception.
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory does not exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }


            // Get the file contents of the directory to copy.
            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                // Create the path to the new copy of the file.
                string temppath = Path.Combine(destDirName, file.Name);

                // Copy the file.
                file.CopyTo(temppath, false);
            }

            // If copySubDirs is true, copy the subdirectories.
            if (copySubDirs)
            {

                foreach (DirectoryInfo subdir in dirs)
                {
                    // Create the subdirectory.
                    string temppath = Path.Combine(destDirName, subdir.Name);

                    // Copy the subdirectories.
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
    }
    public class messageCall
    {
        //public static void infoMessage(int numInfo)
        //{
        //    //MessageBox.Show("", "Title", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //    switch (numInfo)
        //    {
        //        case 1:
        //            MessageBox.Show("", "Title", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //            break;
        //    }
        //}

        public static void errorMessage(int numError)
        {
            //MessageBox.Show("", "Title", MessageBoxButtons.OK, MessageBoxIcon.Error);
            switch (numError)
            {
                case 1:
                    MessageBox.Show("Check your internet connection\nConnect Failure", "Connect Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case 2:
                    MessageBox.Show("Connection to the resource was closed", "Connection Closed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case 3:
                    MessageBox.Show("Server close your connection", "Keep Alive Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case 4:
                    MessageBox.Show("Request extended size limit", "Message Length Limit Exceeded", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case 5:
                    MessageBox.Show("DNS service cannot compare name host with IP adress", "Name Resolution Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case 6:
                    MessageBox.Show("The request was terminated, but an error occurred at the protocol level, for example, the requested resource was not found", "Protocol Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case 7:
                    MessageBox.Show("Full response was not received from the remote server", "Receive Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case 8:
                    MessageBox.Show("Request was canceled", "Request Canceled", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case 9:
                    MessageBox.Show("An error occurred while establishing a connection using SSL", "Secure Channel Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case 10:
                    MessageBox.Show("The full request was not sent to the remote server", "Send Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case 11:
                    MessageBox.Show("The server response was not a valid HTTP response", "Server Protocol Violation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case 12:
                    MessageBox.Show("Response was not received within a certain time", "Timeout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case 13:
                    MessageBox.Show("Server certificate can not be verified", "Trust Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case 14:
                    MessageBox.Show("An exception of an unknown type occurred", "Unknown Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }
    }
}

﻿using System;
using System.Text;
using System.Threading;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Net;

namespace NET_Launcher.Classes
{
    class Vars
    {
        INIManager manager = new INIManager();

        //Paths
        public static string conPath = Application.StartupPath + "\\Launcher\\Content\\";
        public static string gifPath = Application.StartupPath + "\\Launcher\\Gif\\";
        public static string exePath = Application.StartupPath + "\\KD.exe";
        public static string cfgPath = Application.StartupPath + "\\KD.ini";
        public static string lnrPath = Application.StartupPath + "\\Resources\\Launcher.ini";
        public static string rscPath = Application.StartupPath + "\\Resources\\";
        public static string engExePath = Application.StartupPath + "\\KD_ENG.exe";
        public static string japExePath = Application.StartupPath + "\\KD_JAP.exe";
        public static string charPath = Application.StartupPath + "\\CharData.txt";
        public static string movePath = Application.StartupPath + "\\Resources\\moveset.txt";

        //Files
        public static string engExeFile = Path.GetFileName(engExePath);
        public static string japExeFile = Path.GetFileName(japExePath);
        public static string exeFile = Path.GetFileName(exePath);
        public static string cfgFile = Path.GetFileName(cfgPath);
        public static string lnrFile = Path.GetFileName(lnrPath);
        public static string chrFile = Path.GetFileName(charPath);
        public static string mveFile = Path.GetFileName(movePath);
        
        //URL's
        public static Uri en_Link = new Uri(Methods.Mediafire("http://www.mediafire.com/file/bjx0orprxh88qtq/KD_ENG.zip/"));
        public static Uri jp_Link = new Uri(Methods.Mediafire("http://www.mediafire.com/file/wdy1s348lso454v/KD_JAP.zip/"));
        public static Uri enExe_Link = new Uri(Methods.Mediafire("http://www.mediafire.com/file/h7por5aiycflpsz/KD_ENG.exe/"));
        public static Uri jpExe_Link = new Uri(Methods.Mediafire("http://www.mediafire.com/file/sag730u4ebpjc47/KD_JAP.exe/"));
        public static Uri config_Link = new Uri(Methods.Mediafire("http://www.mediafire.com/file/6p15t31t78t6g5w/KD.ini/"));
        public static Uri nightlyremoteV = new Uri(Methods.Mediafire("http://www.mediafire.com/file/nosksxic28jj701/CurrentNightly.txt/"));

        //Other vars
        public static int buttonNum;
        public static int selectedPack;
        public static bool enableBool = false;
        public static bool playBool = false;
        public static float releaselocalV = 0.700F;
        public static float nightlylocalV = 0.701F;
        public static string welcomeVar = Properties.Settings.Default.welcomeText;
        public static string KeyData;
        public static string resultString;
        public static string Key;
        public static string Kdata;
        public static string packPath = Application.StartupPath;
    }
}
